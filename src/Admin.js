import React, { useEffect, useGlobal } from "reactn";
import { Route, Switch } from 'react-router-dom';
import { Layout } from 'antd';
import SideMenu from './layout/SideMenu';
import HeaderBar from './layout/HeaderBar';


import {
  Dashboard, 
  Survey, SurveyDetail,
  PushNotification, PushNotificationDetail,
  Banner, BannerDetail,
  Report,
  RegisteredUser, RegisteredUserDetail,
  Coordinate, CoordinateDetail,
  Collection,CollectionDetail,
  RequestAndComplaint,RequestAndComplaintDetail,
  RequestAndComplaintCategories,RequestAndComplaintCategoriesDetail,
  RequestAndComplaintSubCategoriesList,RequestAndComplaintSubCategoriesDetail,
  NotFoundPage
} from './pages';



import Threats from './pages/Theats';

const { Header, Content } = Layout;

const Admin = (props) => {
  let [token, setToken] = useGlobal('token');
  let [user] = useGlobal('user');
  let [isSmall, setIsSmall] = useGlobal('isSmall')
  let [collapsed, setCollapsed] = useGlobal('collapsed');

  useEffect(() => {
    window.onresize = () => {
      setIsSmall(window.innerWidth < 1024)
    }
  }, [setIsSmall, setToken])

  useEffect(() => {
    if (isSmall)
      setCollapsed(true)
    else
      setCollapsed(false)
  }, [isSmall, setCollapsed])

  if (!token || !user) {
    props.history.push('/login');
  }


  return (
    <Layout >
      <SideMenu collapsed={collapsed} />
      <Layout className="site-layout" style={{ minHeight: '100vh' }} >
        <Header
          className="site-layout-background header"
          style={{ padding: '0 20px' }}
          theme="dark"
        >
          <HeaderBar />
        </Header>

        <Content className="main-wrap">
          <Switch>
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/" component={RegisteredUser} />

            <Route path="/threats" component={Threats} />

           

            <Route exact path="/survey" component={Survey} />
            <Route exact path="/survey/edit/:id" component={SurveyDetail} />
            <Route exact path="/survey/add" component={SurveyDetail} />

            <Route exact path="/coordinate" component={Coordinate} />
            <Route exact path="/coordinate/add" component={CoordinateDetail} />
            <Route exact path="/coordinate/edit/:id" component={CoordinateDetail} />

            <Route exact path="/pushNotifications" component={PushNotification} />
            <Route exact path="/pushNotifications/edit/:id" component={PushNotificationDetail} />
            <Route exact path="/pushNotifications/add" component={PushNotificationDetail} />

            <Route exact path="/banners" component={Banner} />
            <Route exact path="/banners/edit/:id" component={BannerDetail} />
            <Route exact path="/banners/add" component={BannerDetail} />

            <Route exact path="/reports" component={Report} />

            <Route exact path="/registeredUsers" component={RegisteredUser} />
            <Route exact path="/registeredUsers/edit/:id" component={RegisteredUserDetail} />
            <Route exact path="/registeredUsers/add" component={RegisteredUserDetail} />

            <Route exact path="/requestAndComplaint" component={RequestAndComplaint} />
            <Route exact path="/requestAndComplaint/detail/:id" component={RequestAndComplaintDetail} />

            <Route exact path="/requestAndComplaintCategories" component={RequestAndComplaintCategories} />
            <Route exact path="/requestAndComplaintCategories/edit/:id" component={RequestAndComplaintCategoriesDetail} />
            <Route exact path="/requestAndComplaintCategories/add" component={RequestAndComplaintCategoriesDetail} />
            
            <Route exact path="/requestAndComplaintCategories/detail/:id" component={RequestAndComplaintSubCategoriesList} />
            <Route exact path="/requestAndComplaintCategories/detail/:id/add" component={RequestAndComplaintSubCategoriesDetail} />
            <Route exact path="/requestAndComplaintCategories/detail/:id/edit/:subid" component={RequestAndComplaintSubCategoriesDetail} />
          
            <Route exact path="/collection/" component={Collection} />
            <Route exact path="/collection/detail/:id" component={CollectionDetail} />


            <Route path='*' component={NotFoundPage} />

          </Switch>
        </Content>
      </Layout>
    </Layout>
  );
};




export default Admin;
