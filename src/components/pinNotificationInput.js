import React from 'react';
import {Switch, Form, Alert} from 'antd';

export default (props) => {
	let {record, setRecord, name} = props;

	return <Form.Item >

             {record._id
                ?
                    record.pin?<Alert type="success" message={"Bildirim sabitlenmiş!"} banner />:
                                <Alert type="info" message="Bildirim sabitlenmemiş!" banner />
                :
                    <Alert  message="'Sabit Bildirim' alanını daha sonra düzenleyemezsiniz !  ''Bu alanı seçtiğinizde bildirim gitmeyecek, yanlızca listede gözükecek !  ''Sabit bildirimler grup bazlı değildir.!!" banner />} 

        <Switch checked={record[name]} disabled={record._id?true:false}
        checkedChildren="Olsun" unCheckedChildren="Olmasın"
			onChange={v => setRecord({...record,[name]:v})} />
            
              
	</Form.Item>;
};