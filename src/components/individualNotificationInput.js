import React from 'react';
import {Switch, Form, Alert} from 'antd';

export default (props) => {
	let {record, setRecord, name} = props;

	return <Form.Item >

             {record._id
                ?
                    record.isIndividual?<Alert type="success" message={" Bireysel bildirim gönderilmiş!"} banner />:
                                <Alert type="info" message="Bireysel bildirim gönderilmemiş!" banner />
                :
                    <Alert  message="'Bireysel Bildirim' alanını daha sonra düzenleyemezsiniz!" banner />} 

        <Switch checked={record[name]} disabled={record._id?true:false}
        checkedChildren="Gönder" unCheckedChildren="Gönderme"
			onChange={v => setRecord({...record,[name]:v})} />
            
              
	</Form.Item>;
};