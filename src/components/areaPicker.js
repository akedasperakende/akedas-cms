import React, { useMemo, useState } from 'react';
import {Select} from 'antd';
import api from '../service/index'
import qs from 'qs';

export default (props) => {
	let {record, setRecord, name} = props;
	let [location,setSession] = useState([]);
	let _params = {sort: "-area", page: 1, pageSize: 100 };

	useMemo(() => {
		api.get(`/rest/location?${qs.stringify(_params)}`).then(({ data }) => {
			let dbSessions = data.result.rows.sort((a,b) => a.area.localeCompare(b.area))
			setSession(dbSessions);
		})
	}, [])

	return <Select 
	disabled={record._id?true:false} value={record[name] || [] } mode='multiple' placeholder="Bölge seçin"
			onChange={v => setRecord({...record, [name]: v })}>
			{(location) && location.map(b =>
				<Select.Option key={b._id} value={b._id} >{b.area}</Select.Option>)}
		</Select>;
};