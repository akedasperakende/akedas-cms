import React, { useState, useEffect } from 'react';
import { Button, Form, Icon, Upload, Spin, Typography } from 'antd';
import { useUpload } from './../service/useUpload';
import { PlusOutlined, RightOutlined, LeftOutlined, DeleteOutlined } from '@ant-design/icons';

export default (props) => {
	let { record, setRecord, name, title, disabled, errors, required, multi, ext } = props;
	let [upload, uploadResult, uploading, error, clearUpload] = useUpload();
	let [files, setFiles] = useState([]);
	let [fileList, setFileList] = useState([])

	useEffect(() => {

		if (files.length === 0) return;
		if (upload == null) return;
		upload(files);
		setFiles([]);
	}, [upload, files]);

	useEffect(() => {
		if (uploadResult == null) return;
		setRecord({
			...record,
			[name]: multi ? [...record[name], ...uploadResult] : uploadResult[0]
		});
		clearUpload();
	}, [record, uploadResult, clearUpload, multi, name, setRecord]);

	let remove = item => {
		if (multi)
			setRecord({ ...record, [name]: record[name].filter(x => x._id !== item.file.uid) });
		else
			setRecord({ ...record, [name]: null });
	};

	return <>
		<Upload
			accept={ext} listType="picture-card"
			fileList={record[name].map(f => ({
				uid: f._id,
				url: f.url,
				status: 'done'
		}))} onChange={f => remove(f)} beforeUpload={(file) => { setFiles([...files, file]); return false; }}
			multiple={multi} >
			{<Typography.Text type="success">{files.name}</Typography.Text>}
			{files.length >= 8 ? null : <div> <PlusOutlined /> <div style={{ marginTop: 8 }}>Yükle</div> </div>}

			{uploading && <Spin />}
		</Upload>
	</>
};
