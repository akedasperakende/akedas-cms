import React, { useMemo, useState } from 'react';
import { Select } from 'antd';
import api from '../service/index'
import qs from 'qs';
import { useEffect } from 'reactn';

export default (props) => {
	let { record, setRecord, name, disabled, areaChange } = props;
	let [items, setItems] = useState([]);
	let [value, setValue] = useState(record[name] ? { defaultValue: record[name] } : { value: record[name] });
	let _params = { sort: "-name", page: 1, pageSize: 100000, lang: "TR", };
	let [loading, setLoading] = useState(false)

	useMemo(() => {
		setValue(record[name] && !areaChange ? { defaultValue: record[name] } : { value: record[name] })
	}, [record.area, record[name]])

	useEffect(() => {
		if (areaChange)
			setRecord({ ...record, [name]: null })
			
			_params = { ..._params }
			if(record.area) {
				let searcText=	record.area.join(',')
				_params["search"] = searcText;
				_params["searchFields"] = "_id";
			  }
			api.get(`/rest/location?${qs.stringify(_params)}`).then(({ data }) => {
				let concatİtems=[]
				data.result.rows.map((x)=>{
					concatİtems.push(...x.district)
					
				})
				setItems(concatİtems);
				
				setLoading(false)
			})
	}, [record.area])

	return <Select showSearch loading={loading} 
	disabled={record._id?true:disabled}{...value} mode='multiple' placeholder="İlçe Seçin" optionFilterProp="children"
		filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
		onChange={v => setRecord({ ...record, [name]: v })}>
		{items && items.map(b =>
			<Select.Option key={b._id} value={b._id} >{b.name}</Select.Option>)}
	</Select>
};