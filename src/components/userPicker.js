import React, { useState, useEffect, useMemo } from 'react';
import { Select } from 'antd';
import api from '../service/index'
import qs from 'qs';

export default (props) => {
	let { record, setRecord, name } = props;
	let [user, setUser] = useState([]);
	let _params = { query: {deleted: { $ne: true }}, sort: "-createdAt", page: 1, pageSize: 100000 };

	useEffect(() => {
		api.get(`/rest/users?${qs.stringify(_params)}`).then(({ data }) => {
			let dbUser = data.result.rows.sort((a, b) => a.name.localeCompare(b.name))
			setUser(dbUser);
		})
	}, [])

	let mentionedUsers = (v) => {
		let users = v.map(x => {
			let mentionedUser = user.find(e => e._id === x)
			return ({ _id: mentionedUser._id, name: mentionedUser.name, lastname: mentionedUser.lastname, avatar: mentionedUser.avatar, isSelected: true });
		})
		setRecord({ ...record, [name]: users })
	}


	return <Select defaultValue={record[name]? record[name].map(e => e._id) : []} mode='multiple' placeholder="Kullanıcı seçin"
		showSearch={true}
		onChange={mentionedUsers}
		 filterOption={(input, option) =>
       		option.props.children[0].toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
	   		option.props.children[2].toLowerCase().indexOf(input.toLowerCase()) >= 0 }>
				   
		{(user) && user.map(b =>
			<Select.Option key={b._id} value={b._id} >{b.name} {b.lastname}</Select.Option>)}
	</Select>;
};
