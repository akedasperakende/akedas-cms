import React, { useState, useEffect } from 'react';
import { PlusOutlined,LoadingOutlined} from '@ant-design/icons';
import api from '../service/index'
import { Upload,message, Progress } from 'antd';

export default (props) => {
	let { record, setRecord, name } = props;
	let [file, setFile] = useState([]);
	let [imageUrl, setImageUrl] = useState(false);
	let [loading, setLoading] = useState(false)
	let [uploadStart, setUploadStart] = useState(false)
	const [progress, setProgress] = useState(0);
	useEffect(() => {
		if(record[name]) {
			setImageUrl(record[name]);
		}
	}, []);

	let handleChange = info => {
		if(!info.event && uploadStart === false) {
			setUploadStart(true)
			const formData = new FormData();
			formData.append('files_0', info.file.originFileObj)
			api.post("/api/upload", formData, { headers: { 'ContenType': 'multipart/form-data'},
			onUploadProgress: event => {
				const percent = Math.floor((event.loaded / event.total) * 100);
				setProgress(percent-1);
			} }).then(({ data: { result, result_message } }) => {
				setTimeout(() => setProgress(100), 1000);
				setTimeout(() => setProgress(0), 2000);
				if (result_message.type === 'success') {
					setFile(result[0]);
					setImageUrl(result[0].url);
					setRecord({...record, [name]:result[0].url})
					setUploadStart(false)
				}
				setLoading(false);
			});
		}
	  };

	let beforeUpload = () => { 
		setImageUrl(false)
		setLoading(true);
	}  
	const uploadButton = (
		<div>
		  {loading ? <LoadingOutlined /> : <PlusOutlined />}
		  <div style={{ marginTop: 8 }}>Upload</div>
		</div>
	);

	return <>
		<Upload
        name={name}
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
		beforeUpload={beforeUpload}
        onChange={handleChange}
      >
        {imageUrl ? <img src={imageUrl} alt={name} style={{ width: '100%', height:'inherit', objectFit:'contain' }}  /> : uploadButton}
      </Upload>
	  {progress > 0 ? <Progress style={{width:'60%'}} percent={progress} /> : null}
	</>
};
