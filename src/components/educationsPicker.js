import React, { useMemo, useState} from 'react';
import {Select} from 'antd';
import api from '../service/index'
import qs from 'qs';

export default (props) => {
	let {record, setRecord, name} = props;
	let [educations,setEducation] = useState([]);
	let _params = {sort: "-createdAt", page: 1, pageSize: 100 };

	useMemo(() => {
		api.get(`/rest/educations?${qs.stringify(_params)}`).then(({ data }) => {
			let dbEducations = data.result.rows.sort((a,b) => a.name.localeCompare(b.name))
			setEducation(dbEducations);
		})
	}, [_params])

	return <Select value={record[name] || [] } mode='single' placeholder="Eğitim seçin"
			onChange={v => setRecord({...record, [name]: v })}>
			{(educations) && educations.map(b =>
				<Select.Option key={b._id} value={b._id} >{b.name}</Select.Option>)}
		</Select>;
};