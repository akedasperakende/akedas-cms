import React, { useMemo, useState } from 'react';
import { Select } from 'antd';
import api from '../service/index'
import qs from 'qs';
import { useEffect } from 'reactn';

export default (props) => {
	let { record, setRecord, name, disabled, moduleChange } = props;
	let [items, setItems] = useState([]);
	let [value, setValue] = useState(record[name] ? { defaultValue: record[name] } : { value: record[name] });
	let _params = { sort: "-createdAt", page: 1, pageSize: 100, lang: "TR", appShow: "true" };
	let [loading, setLoading] = useState(false)
	let [moduleShowParameters, setModuleShowParameters] = useState([])
	
	let replaceModuleName = [
		{ name: "survey", realName: "surveys", showParameters: ["name"], isGetData: true },
		{ name: "coordinate", realName: "bloodNeeds", showParameters: ["bloodName", "contactPerson"], isGetData: false },
		{ name: "user", realName: "attendess", showParameters: ["name"], isGetData: false },

	]

	useMemo(() => {
		setValue(record[name] && !moduleChange ? { defaultValue: record[name] } : { value: record[name] })
	}, [record.moduleId, record[name]])

	useEffect(() => {
		if (moduleChange)
			setRecord({ ...record, [name]: null })
		let moduleDetail = replaceModuleName.find(f => f.name === record.moduleId)
		if (moduleDetail && moduleDetail.isGetData) {
			setModuleShowParameters(moduleDetail.showParameters)
			setLoading(true)
			if (moduleDetail.params)
				_params = { ..._params, ...moduleDetail.params }
			api.get(`/rest/${moduleDetail.realName}?${qs.stringify(_params)}`).then(({ data }) => {
				setItems(data.result.rows);
				setLoading(false)
			})
		}
		else setItems([])
	}, [record.moduleId])

	return <Select showSearch loading={loading} disabled={disabled} {...value} placeholder="Paylaşım Seçin" optionFilterProp="children"
		filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
		onChange={v => setRecord({ ...record, [name]: v })}>
		{items && items.map(b =>
			<Select.Option key={b._id} value={b._id} disabled={!b.active} >{moduleShowParameters.map((m) => b[m]).join(" - ")}</Select.Option>)}
	</Select>
};