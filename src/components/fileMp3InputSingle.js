import React, { useState, useEffect } from 'react';
import {  Upload} from 'antd';
import { PlusOutlined,LoadingOutlined,UploadOutlined} from '@ant-design/icons';
import api from '../service/index'

import {  Button } from 'antd';

export default (props) => {
	let { record, setRecord, name ,disabled, ext} = props;
	let [file, setFile] = useState([]);
	let [imageUrl, setImageUrl] = useState(false);
	let [loading, setLoading] = useState(false)
	let [uploadStart, setUploadStart] = useState(false)
	
	useEffect(() => {
		if(record[name]) {
			setImageUrl(record[name].url);
		}
	}, []);

	let handleChange = info => {
		if(!info.event && uploadStart === false) {
			setUploadStart(true)
			const formData = new FormData();
			formData.append('files_0', info.file.originFileObj)
			api.post("/api/upload", formData, { headers: { 'ContenType': 'multipart/form-data'} }).then(({ data: { result, result_message } }) => {
				if (result_message.type === 'success') {
					delete result[0].width
					delete result[0].height
					setFile(result[0]);
					setImageUrl(result[0].url);
					setRecord({...record, [name]:result[0]})
					setUploadStart(false)
				}
				setLoading(false);
			});
		}
	  };

	let beforeUpload = () => { 
		setImageUrl(false)
		setLoading(true);
	}  
	const uploadButton = (
		<div  >
			<Button icon= {loading ? <LoadingOutlined /> : <PlusOutlined />}>Upload</Button>
		</div>
	);

	return <>
		<Upload
        name={name}
		accept={ext}
        listType="text"
        className="avatar-uploader"
        showUploadList={false}
		beforeUpload={beforeUpload}
        onChange={handleChange}
		disabled={disabled}
      >
	  {imageUrl ? <audio controls src={imageUrl} alt={name} style={{ objectFit: 'cover', width: 360 }}  /> : uploadButton}
      </Upload>
	</>
};
