import React, { useState, useMemo } from 'react';
import {Select} from 'antd';
import api from '../service/index'
import qs from 'qs';

export default (props) => {
	let {record, setRecord, name, mode} = props;
	let [types, setTypes] = useState([]);
	let _params = {sort: "-createdAt", page: 1, pageSize: 100 };


	useMemo(() => {
		api.get(`/rest/secondhandEstateTypes?${qs.stringify(_params)}`).then(({ data }) => {
			let dbTypes = data.result.rows.sort((a,b) => a.name.localeCompare(b.name))
			setTypes(dbTypes);
		})
	}, [])


	return <Select value={record[name] || [] }  mode={mode} placeholder="Emlak Türü Seçin."  
			    onChange={v => setRecord({...record, [name]: v })}>
			    {(types) && types.map(b =>
				<Select.Option key={b.name} value={b.name} >{b.name}</Select.Option>)}
		    </Select>
};