import React from 'react';
import {Select} from 'antd';

export default (props) => {
	let {record, setRecord, name, mode} = props;
	let _params = {sort: "-createdAt", page: 1, pageSize: 100 };

	return <Select value={record[name] || [] }  mode={mode} placeholder="Emlak ilan Tipi Seçin."  
			    onChange={v => setRecord({...record, [name]: v })}>
			<Select.Option value="Satılık">Satılık</Select.Option>
			<Select.Option value="Kiralık">Kiralık</Select.Option>
		    </Select>
};