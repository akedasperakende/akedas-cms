import React, { useMemo, useState } from 'react';
import {Select} from 'antd';
import api from '../service/index'
import qs from 'qs';

export default (props) => {
	let {record, setRecord, name} = props;
	let [lessons,setLesson] = useState([]);
	let _params = {sort: "-createdAt", page: 1, pageSize: 100 };

	useMemo(() => {
		api.get(`/rest/lessons?${qs.stringify(_params)}`).then(({ data }) => {
			let dbLessons = data.result.rows.sort((a,b) => a.name.localeCompare(b.name))
			setLesson(dbLessons);
		})
	}, [])

	return <Select value={record[name] || [] } mode='multiple' placeholder="Ders seçin"
			onChange={v => setRecord({...record, [name]: v })}>
			{(lessons) && lessons.map(b =>
				<Select.Option key={b._id} value={b._id} >{b.name}</Select.Option>)}
		</Select>;
};