import React, { useMemo, useState } from 'react';
import { Select } from 'antd';
import api from '../service/index'
import qs from 'qs';

export default (props) => {
	let { record, setRecord, name, moduleChange="1",setModuleChange } = props;
	let [modules, setModules] = useState([]);
	let _params = { sort: "order", page: 1, pageSize: 100, lang: "TR", appShow: "true" };

	useMemo(() => {
		api.get(`/rest/modules?${qs.stringify(_params)}`).then(({ data }) => {
			let dbModules = data.result.rows.sort((a, b) => a.name.localeCompare(b.name))
			setModules(dbModules);
		})
	}, [])

	return <Select value={record[name] || []} placeholder="Modul seçin"
		onChange={v => { if(moduleChange!=="1")setModuleChange(true); setRecord({ ...record, [name]: v }) }}>
		<Select.Option key="not_modul" value="not_modul" >Module gitmesin.</Select.Option>
		<Select.Option key="notifications" value="notifications" >Bildirimler</Select.Option>
		{(modules) && modules.map(b =>
			<Select.Option key={b.refId} value={b.refId} >{b.name}</Select.Option>)}
	</Select>;
};