import React, { useState, useEffect } from 'react';
import { Upload,message, Progress } from 'antd';
import { PlusOutlined,LoadingOutlined, FilePdfOutlined}from '@ant-design/icons';
import api from '../service/index'

export default (props) => {
	let { record, setRecord, name ,disabled, resize} = props;
	let [file, setFile] = useState([]);
	let [imageUrl, setImageUrl] = useState(false);
	let [loading, setLoading] = useState(false)
	let [uploadStart, setUploadStart] = useState(false)
	const [progress, setProgress] = useState(0);
	useEffect(() => {
		if(record[name]) {
			setImageUrl(record[name].url);
		}
	}, []);

	let handleChange = info => {
		if(!info.event && uploadStart === false) {
			setUploadStart(true)
			const formData = new FormData();
			formData.append('files_0', info.file.originFileObj)
			formData.append("resize",resize)
			api.post("/api/upload", formData, { headers: { 'ContenType': 'multipart/form-data'},
			onUploadProgress: event => {
				const percent = Math.floor((event.loaded / event.total) * 100);
				setProgress(percent-1);
			} }).then(({ data: { result, result_message } }) => {
				console.log(result)
				setTimeout(() => setProgress(100), 1000);
				setTimeout(() => setProgress(0), 2000);
			console.log(result_message.type)
				if (result_message.type === 'success') {
					// delete result[0].width
					// delete result[0].height
					setFile(result[0]);
					setImageUrl(result[0].url);
					setRecord({...record, [name]:result[0]})
					setUploadStart(false)
				}
				setLoading(false);
			});
		}
	  };

	let beforeUpload = () => { 
		setImageUrl(false);
		setLoading(true);
	}  
	const uploadButton = (
		<div  >
		  {loading ? <LoadingOutlined /> : <PlusOutlined />}
		  <div style={{ marginTop: 8 }}>Upload</div>
		</div>
	);

	if(name=="pdf"){
		return <>
		<Upload
        name={name}
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
		beforeUpload={beforeUpload}
        onChange={handleChange}
		disabled={disabled}
      >

        {imageUrl ? <FilePdfOutlined style={{ fontSize: '50px' }} /> :  uploadButton}
      
      </Upload>
	</>
	}
	else{
		return <>
		<Upload
		accept="image/*"
        name={name}
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
		beforeUpload={beforeUpload}
        onChange={handleChange}
		disabled={disabled}
      >
        {imageUrl ? <img src={imageUrl} alt={name} style={{ width: '100%', height:'inherit', objectFit:'contain' }}  /> : uploadButton}
      </Upload>
	  {progress > 0 ? <Progress style={{width:'60%'}} percent={progress} /> : null}
	</>
	}
};
