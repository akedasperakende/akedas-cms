import React from 'react';
import {Switch, Form, Alert} from 'antd';

export default (props) => {
	let {record, setRecord, name} = props;

	return <Form.Item >

             {record._id
                ?
                    record.sendNow?<Alert type="success" message={"Hemen Gönder seçilmiş! Bildirim gönderilmiştir!."} banner />:
                                <Alert type="info" message="Hemen Gönder seçilmemiş!!" banner />
                :
                    <Alert  message="Acil durumlar dışında kullanmayınız! 'Hemen Gönder' alanını daha sonra düzenleyemezsiniz !'" banner />} 

        <Switch checked={record[name]} disabled={record._id?true:false}
        checkedChildren="Aktif" unCheckedChildren="Pasif"
			onChange={v => setRecord({...record,[name]:v})} />
            
              
	</Form.Item>;
};