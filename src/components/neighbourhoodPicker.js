import React, { useMemo, useState } from 'react';
import { Select } from 'antd';
import api from '../service/index';
import qs from 'qs';
import { useEffect } from 'reactn';

export default (props) => {
  let { record, setRecord, name, disabled, districtChange } = props;
  let [items, setItems] = useState([]);
  let [value, setValue] = useState(
    record[name] ? { defaultValue: record[name] } : { value: record[name] },
  );
  let _params = { sort: '-name', page: 1, pageSize: 100000, lang: 'TR' };
  let [loading, setLoading] = useState(false);

  useMemo(() => {
    setValue(
      record[name] && !districtChange
        ? { defaultValue: record[name] }
        : { value: record[name] },
    );
  }, [record.district, record[name]]);

  useEffect(() => {
    if (districtChange) setRecord({ ...record, [name]: null });

    let district = record.district;
    _params = { ..._params };
    if (record.district) {
      let searcText = record.area.join(',');
      _params['search'] = searcText;
      _params['searchFields'] = '_id';
    }
    api.get(`/rest/location?${qs.stringify(_params)}`).then(({ data }) => {
      let concatItems = [];
      data.result.rows.map((x) => {
        x.district.map((x) => {
          let found = district && district.find((element) => element == x._id);
          if (found) concatItems.push(...x.neighbourhood);
        });
      });
      setItems(concatItems);

      setLoading(false);
    });
  }, [record.district]);

  return (
    <Select
      showSearch
      loading={loading}
      disabled={record._id?true:disabled}
	  mode='multiple'
      {...value}
      placeholder="Mahalle Seçin"
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
      onChange={(v) => setRecord({ ...record, [name]: v })}
    >
      {items &&
        items.map((b) => (
          <Select.Option key={b._id} value={b._id}>
            {b.name}
          </Select.Option>
        ))}
    </Select>
  );
};
