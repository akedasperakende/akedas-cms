import React, {useEffect, useState, useGlobal} from "reactn";
import {useParams} from 'react-router-dom'
import moment from 'moment';
import {Row, Col, Form, Input, Button, Switch, DatePicker, Card, message, InputNumber, Alert} from 'antd';
import {useHistory} from 'react-router-dom';
import {LeftOutlined} from "@ant-design/icons";
import {FileInputSingle} from '../components';

import api from '../service/index'
import {Link} from 'react-router-dom';
import {useCallback} from "react";


const BannerDetail = (props) => {
    let params = useParams()
    let history = useHistory()
    let id = params.id !== "add" ? params.id : false;
    let [langs] = useGlobal("langs")
    let [isSmall, setIsSmall] = useGlobal('isSmall')

    let newRecord = {
        lang: langs ? langs.code : "TR",
        title: '',
        item_id: null,
        active: true,
        order: 0,
        startDate: new Date(),
        endDate: new Date(),
        media: {},
        isLink:false,
        moduleId:"not_modul"
    }
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);

    let [data, setData] = useState(id ? [] : newRecord);
    let [errors, setErrors] = useState([]);
    let [moduleChange, setModuleChange] = useState(false);
    let [loading, setLoading] = useState(id ? true : false);
    let [validationCheck, setValidationCheck] = useState(false)

    let [modules] = useGlobal("modules");
    let path = props.location.pathname.split('/')[1];
    let module
    if (modules !== null) {
        [module] = modules.filter(el => el._id === path);
    }

    //onetime run
    useEffect(() => {
        if (modules && id) {
            setLoading(true)
            api.get("/rest/banners/" + id).then(({data: {result, result_message}}) => {
                setData(result);
                setLoading(false)
            });
        }
    }, [id]);

    let validate = useCallback(() => {
        let errors = {};

        if (data.lang == null)
            errors.lang = 'Zorunlu Alan!'

        if (data.order == null)
            errors.order = 'Zorunlu Alan!'

        if (data.media == null)
            errors.media = 'Zorunlu Alan!'

        // if (data.moduleId == null)
        //   errors.moduleId = 'Zorunlu Alan!'

        if (data.startDate == null)
            errors.startDate = 'Zorunlu Alan!'

        if (data.endDate == null)
            errors.endDate = 'Zorunlu Alan!'

        errors.all = Object.getOwnPropertyNames(errors).map(n => errors[n]);
        errors.hasError = errors.all.length > 0;
        return errors;
    }, [data]);

    useEffect(() => {
        if (validationCheck) setErrors(validate())
    }, [validationCheck, data, validate]);


    let save = async () => {
        let err = validate()
        if (err.hasError) {
            setErrors(err)
            window.scrollTo({top: 20, behavior: 'smooth'});
        } else {
            if (id) {
                api.put("/rest/banners/" + id, data).then(({data: {result, result_message}}) => {
                    if (result_message.type === 'success') {
                        message.success("Bilgiler güncellendi", 2);
                        history.push('/banners')
                    } else
                        message.error("Kayıt eklenirken bir hata oluştu.", 2);
                })
            } else {
                api.post("/rest/banners", data).then(({data: {result, result_message}}) => {
                    if (result_message.type === 'success') {
                        message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
                        history.push('/banners')
                    } else {
                        message.error(result_message.message, 2);
                    }
                })
            }
        }
    };
    function disableDateRanges(range = { startDate: false, endDate: false }) {
        const { startDate, endDate } = range;
        return function disabledDate(current) {
          let startCheck = true;
          let endCheck = true;
          if (startDate) {
            startCheck = current && current < moment(startDate, 'YYYY-MM-DD');
          }
          if (endDate) {
            endCheck = current && current > moment(endDate, 'YYYY-MM-DD');
          }
          return (startDate && startCheck) || (endDate && endCheck);
        };
      }
      function disabledDate(current) {
        return current < moment();
      }
    return (
        <div>
            <div className="list-head">
                <div className="list-title">
                    <h1>{module ? module.name : ""}</h1>
                </div>
                <div className="list-buttons">
                    <Link to="/banners">
                        <Button type="light" icon={<LeftOutlined/>} size="large">{!isSmall &&"GERİ"}</Button>
                    </Link>
                </div>
            </div>
            <div className="form-wrap">
                <Card title={id ? "Düzenle" : "Ekle"} loading={loading}>
                    <Form layout="horizontal" size={"large"} onFinish={save}>
                        {/* <Row direction="row">
                            <Col xs={{span: 24}} md={{span: 12}}>
                                <Form.Item label="Dil">
                                    <LangPicker record={data} setRecord={setData} name="lang"/>
                                </Form.Item>
                            </Col>
                        </Row> */}
                        <Row direction="row">
                            <Col xs={{span: 24}} md={{span: 24}}>
                                <Form.Item label="İsim" help={errors.title}
                                           validateStatus={errors.title ? 'error' : 'success'}>
                                    <Input name="title" value={data.title}
                                           onChange={e => setData({...data, title: e.target.value})}/>
                                </Form.Item>
                            </Col>
                        </Row>
                        {/* <Row direction="row">
                            <Col xs={{span: 24}} md={{span: 12}}>
                               <Form.Item label="Açılacak Modül" help={errors.moduleId}
                                          validateStatus={errors.moduleId ? 'error' : 'success'}>
                                   <ModulePicker record={data} setRecord={setData} moduleChange={moduleChange}
                                                 setModuleChange={setModuleChange} name="moduleId"/>
                                </Form.Item>
                            </Col>

                        </Row> */}
                        <Row direction="row">
                            <Col xs={{span: 24}} md={{span: 12}}>
                                <Form.Item label="Durumu" help={errors.active}
                                           validateStatus={errors.active ? 'error' : 'success'}>
                                    <Switch checked={data.active ? true : false} checkedChildren="Aktif"
                                            unCheckedChildren="Pasif" onChange={v => setData({...data, active: v})}/>
                                </Form.Item>
                            </Col>
                            <Col xs={{span: 24}} md={{span: 12}}>
                                <Form.Item label="Aktif Link" help={errors.isLink}
                                           validateStatus={errors.isLink ? 'error' : 'success'}>
                                    <Switch checked={data.isLink ? true : false} checkedChildren="Aktif"
                                            unCheckedChildren="Pasif" onChange={v => setData({...data,isLink:v})}/>
                                </Form.Item>
                            </Col>

                            <Col xs={{span: 24}} md={{span: 12}}>
                                <Form.Item label="Sıra" help={errors.order}
                                           validateStatus={errors.order ? 'error' : 'success'}>
                                    <InputNumber value={data.order} min={0} defaultValue={0} style={{width: 100}}
                                                 onChange={v => setData({...data, order: v})}/>
                                </Form.Item>
                            </Col>
                            {
                                data.isLink && (
                                    <Col xs={{span: 24}} md={{span: 12}}>
                                        <Form.Item label="Yönlendirilecek Link" help={errors.redirectLink}
                                                   validateStatus={errors.redirectLink ? 'error' : 'success'}>
                                            <Input name="redirectLink" value={data.redirectLink}
                                                   onChange={e => setData({...data, redirectLink: e.target.value})}/>
                                        </Form.Item>
                                    </Col>
                                )
                            }
                        </Row>
                        <Row direction="row">
                            <Col xs={{span: 24}} md={{span: 12}}>
                                <Form.Item label="Başlangıç Tarihi" required help={errors.startDate}
                                           validateStatus={errors.startDate ? 'error' : 'success'}>
                                    <DatePicker
                                 placeholder= {data.startDate}
                                 defaultValue={() => moment(data.startDate)}
                                 showTimeSelect
                                 format='DD/MM/YYYY HH:mm' showTime={true}
                                 selected={startDate}
                                 selectsStart
                                 startDate={startDate}
                                 endDate={endDate}
                                 disabledDate = { disableDateRanges({startDate,endDate})}
                                 onChange={date => {setData({...data, startDate: date});setStartDate(date)}}
                                 />
                                </Form.Item>
                            </Col>
                            <Col xs={{span: 24}} md={{span: 12}}>
                                <Form.Item label="Bitiş Tarihi" required help={errors.endDate}
                                           validateStatus={errors.endDate ? 'error' : 'success'}>
                                    <DatePicker
                                    placeholder= {data.endDate}
                                    defaultValue={() => moment(data.endDate)}
                                    showTimeSelect
                                    format='DD/MM/YYYY HH:mm' showTime={true}
                                    selected={endDate}
                                    selectsEnd
                                    startDate={startDate}
                                    minDate={startDate}
                                    disabledDate = { disableDateRanges({endDate,startDate})}
                                    onChange={date => {setData({...data, endDate: date});setEndDate(date)}}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row direction="row">
                            <Col span={24}>
                                <Form.Item label="Medya" required help={errors.name}
                                           validateStatus={errors.name ? 'error' : 'success'}>
                                    <FileInputSingle record={data} setRecord={setData} name="media"/>
                                    <Alert message="Banner için en iyi boyut 1920 x 1080 pikseldir." banner/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row direction="row">
                            <Col span={24}>
                                <Form.Item>
                                    <Button type="primary" disabled={loading} htmlType="submit" size="large"
                                            block> KAYDET </Button>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Card>
            </div>
        </div>
    );
};


export default BannerDetail;