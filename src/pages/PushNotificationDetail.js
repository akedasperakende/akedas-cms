import React, { useEffect, useState, useGlobal } from "reactn";

import { useParams } from 'react-router-dom'
import moment from 'moment';
import { Row,Col,Select,Form, Input, Button, Switch, DatePicker, Card, message, Alert} from 'antd';
import { useHistory } from 'react-router-dom';
import { LeftOutlined } from "@ant-design/icons";
import { FileInputSingle, SendNotificationInput, AreaPicker,DistrictPicker,NeighbourhoodPicker,IndividualNotificationInput,SendNowNotificationInput} from '../components';

import api from '../service/index'
import { Link } from 'react-router-dom';
import { useCallback } from "react";

const PushNotificationDetail = (props) => {
  let params = useParams()
  let history = useHistory()
  let id = params.id !== "add" ? params.id : false;
  const { TextArea } = Input;
  let [langs] = useGlobal("langs")
  let [isSmall, setIsSmall] = useGlobal('isSmall')
  
  let newRecord = {
    lang: langs ? langs.code : "TR",
    title: '',
    content: '',
    active: true,
    isIndividual:false,
  //  pin:false,
    date: new Date(),
    area:[],
    district:[],
    neighbourhood:[],
    //type:null
  }

  let [data, setData] = useState(id ? [] : newRecord);
  let [errors, setErrors] = useState([]);
  let [areaChange, setAreaChange] = useState(false);
  let [districtChange, districtChangeChange] = useState(false);
  let [loading, setLoading] = useState(id ? true : false);
  let [validationCheck, setValidationCheck] = useState(false)
  
  let [modules] = useGlobal("modules");
  let path = props.location.pathname.split('/')[1];
  let module
  if(modules !== null){
     [module] = modules.filter(el => el._id === path);
  }

  //onetime run
  useEffect(() => {
    if (modules && id) {
      setLoading(true)
        api.get("/rest/pushNotifications/" + id).then(({ data: { result, result_message } }) => {
          setData(result);   
          setLoading(false)
        });
    }
  }, [id]);

  let validate = useCallback(() => {
    let errors = {};

    if (data.lang === null)
      errors.lang = 'Zorunlu Alan!'

    if (data.title === null || data.title.length === 0)
      errors.title = 'Zorunlu Alan!'

    if (data.content === null || data.content.length === 0)
      errors.content = 'Zorunlu Alan!'

    if (data.date === null)
      errors.date = 'Zorunlu Alan!'

    errors.all = Object.getOwnPropertyNames(errors).map(n => errors[n]);
    errors.hasError = errors.all.length > 0;
    return errors;
  }, [data]);

  useEffect(() => { if (validationCheck) setErrors(validate()) }, [validationCheck, data, validate]);

  let save = async () => {
    setValidationCheck(true)
    let err = validate()
    if (err.hasError) {
      setErrors(err)
      window.scrollTo({ top: 20, behavior: 'smooth' });
    }
    else {
      if(id) {
        api.put("/rest/pushNotifications/" + id, data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Bilgiler güncellendi", 2);
            history.push('/pushNotifications')
          }
          else
            message.error("Kayıt eklenirken bir hata oluştu.", 2);
        })
      } else {
        api.post("/rest/pushNotifications", data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
            history.push('/pushNotifications')
          } else {
            message.error(result_message.message, 2);
          }
        })
      } 
    }        
  };

  return (
    <div>
      <div className="list-head">
        <div className="list-title">
          <h1>{module ? module.name : ""}</h1>
        </div>
        <div className="list-buttons">
          <Link to="/pushNotifications">
            <Button type="light" icon={<LeftOutlined />} size="large">{!isSmall&&"GERİ"}</Button>
          </Link>
        </div>
      </div>
      <div className="form-wrap">
          <Card title={id ? "Düzenle" : "Ekle"}loading={loading}>
            <Form layout="horizontal" size={"large"} onFinish={save}>
              
              {/* <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Dil">
                    <LangPicker record={data} setRecord={setData} name="lang" />
                  </Form.Item>
                </Col>
              </Row>
              */}
              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Uygulama Listesinde Göster" help={errors.active} validateStatus={errors.active ? 'error' : 'success'}>
                    <Switch checked={data.active ? true : false} checkedChildren="Aktif" unCheckedChildren="Pasif" onChange={v => setData({ ...data, active: v })} />
                  </Form.Item>
                </Col>
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Gönderim Zamanı" required help={errors.date} validateStatus={errors.date ? 'error' : 'success'}>
                    <DatePicker defaultValue={() => moment(data.date)} onChange={v => setData({ ...data, date: v })} format='DD/MM/YYYY HH:mm' showTime={true} />
                  </Form.Item>
                </Col>
              </Row>

              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Bildirim Gönder" help={errors.sendNotification} validateStatus={errors.sendNotification ? 'error' : 'success'}>
                    <SendNotificationInput record = {data} setRecord = {setData} name = "sendNotification" />
                  </Form.Item>
                </Col> 
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Hemen Gönder" help={errors.sendNow} validateStatus={errors.sendNow ? 'error' : 'success'}>
                    <SendNowNotificationInput record = {data} setRecord = {setData} name = "sendNow" />
                  </Form.Item>
                </Col> 
                
              </Row>  
              {/* <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Sabit Bildirim" help={errors.pin} validateStatus={errors.pin ? 'error' : 'success'}>
                    <PinNotificationInput record = {data} setRecord = {setData} name = "pin" />
                  </Form.Item>
                </Col>
              </Row> */}
              <Row direction="row">
              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Bireysel Bildirim" help={errors.isIndividual} validateStatus={errors.isIndividual ? 'error' : 'success'}>
                    <IndividualNotificationInput record = {data} setRecord = {setData} name = "isIndividual" />
                  </Form.Item>
                </Col>                 
              </Row>
            
                {data.isIndividual === true &&
              <Row direction="row">
               <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Muhatap No" help={errors.muhatapNo} validateStatus={errors.muhatapNo ? 'error' : 'success'}>
                      <Input name="muhatapNo" value={data.muhatapNo} onChange={e => setData({ ...data, muhatapNo: e.target.value })} />
                    </Form.Item>
                  </Col>
              </Row>
            }{data.isIndividual === false &&
                  <Row direction="row">
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Bölge:" help={errors.area} validateStatus={errors.area ? 'error' : 'success'}>
                      <AreaPicker record={data} setRecord={setData} areaChange={areaChange} name="area" mode="multiple" />
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="İlçe:" help={errors.district} validateStatus={errors.district ? 'error' : 'success'}>
                      <DistrictPicker record={data} setRecord={setData} areaChange={areaChange} districtChange={districtChange} name="district" mode="multiple" 
                    disabled={!(data.area.length>0 )? true : false}/>
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Mahalle:" help={errors.neighbourhood} validateStatus={errors.neighbourhood ? 'error' : 'success'}>
                      <NeighbourhoodPicker record={data} setRecord={setData} districtChange={districtChange} name="neighbourhood" mode="multiple" 
                    disabled={ !(data.district.length>0)  ? true : false}/>
                    </Form.Item>
                  </Col>

              </Row> 
              } 
              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 24 }}>
                  <Form.Item label="Başlık" required help={errors.title} validateStatus={errors.title ? 'error' : 'success'}>
                    <Input name="title" value={data.title} onChange={e => setData({ ...data, title: e.target.value })} />
                  </Form.Item>
                </Col>
              </Row>

              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 24 }}>
                  <Form.Item label="İçerik" required help={errors.content} validateStatus={errors.content ? 'error' : 'success'}>
                    <TextArea name="content" value={data.content} onChange={e => setData({ ...data, content: e.target.value })} />
                  </Form.Item>
                </Col>
              </Row>

              {/* <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Açılacak Modül" required help={errors.type} validateStatus={errors.type ? 'error' : 'success'}>
                    <ModulePicker record={data} setRecord={setData} name="type" />
                  </Form.Item>
                </Col>
              </Row> */}

              <Row direction="row">
                <Col span={12}>
                  <Form.Item label="İcon" help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                    <FileInputSingle record={data} setRecord={setData} name="icon" />
                    <Alert  message="Yüklenecek ikon 80 x 80 çözünürlüğünde olmalıdır." banner />
                  </Form.Item>
                </Col>
              </Row>       
{/* 
              <Row direction="row">
                <Col span={24}>
                  <Form.Item label="Medya" help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                    <FileInputSingle record={data} setRecord={setData} name="media" />
                    <Alert  message="Yüklenecek görüntü bire bir görsel için 200 x 200, bire iki görsel için 1200 x 663 çözünürlüğünde olmalıdır." banner />
                  </Form.Item>
                </Col>
              </Row> */}

              <Row direction="row">
                <Col span={24}>
                  <Form.Item>
                    <Button type="primary" disabled={loading} htmlType="submit" size="large" block > KAYDET </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Card>
      </div>
    </div>
  );
};
export default PushNotificationDetail;
