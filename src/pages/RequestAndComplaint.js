import React, {useEffect, useState, useGlobal} from "reactn";
import {Table, Space, Button, Input,Alert} from 'antd';
import api from '../service/index'
import {Link} from 'react-router-dom';
import qs from 'qs';
import ExportJsonExcel from 'js-export-excel';
import moment from 'moment';
import {
   FileTextOutlined,
   CheckOutlined,
   CloseOutlined,
   Loading3QuartersOutlined,
   AlignCenterOutlined,
   DownloadOutlined
} from "@ant-design/icons";

const RequestAndComplaint = (props) => {

   const {Search} = Input;
   let [data, setData] = useState([]);
   let [totalCount, setTotalCount] = useState(0)
   let [selectedRows, setSelectedRows] = useState([])
   let [loading, setLoading] = useState(false)
   let [unchangedData,setUnchangedData]=useState([])
   let [isSmall, setIsSmall] = useGlobal('isSmall')
     
   let [modules] = useGlobal("modules");
   let path = props.location.pathname.split('/')[1];
   let module
   if (modules !== null) {
      [module] = modules.filter(el => el._id === path);
   }

   let handleTableChange = async (page, filters, sorter) => {
      get(page,filters,sorter);
    }
  
    let get = async (page, filters, sorter) => {
      
      if(page) {
        page =  { page : page.current, pageSize: page.pageSize }
      } else {
        page = { page: 1, pageSize: 10 };
      }
      let shortString = '-transactionNumber';
  
      if (sorter) {
        if(Object.keys(sorter).length) {
            shortString = (sorter.order === 'descend' ? '-' : '') + sorter.field
        }
      }
  
      let _params = {sort: shortString, ...page, };


      let restData = await api.get(`/rest/requestAndComplaint?${qs.stringify(_params)}`, {_params}).then(({data}) => {
         if (data.result) {
        console.log(data.result);
            setLoading(false)
            setTotalCount(data.result.total);
            return data.result.rows.map((item, key) => {
               if (item.solution)
                  item.solution = <CheckOutlined style={{color:"#7cb305", fontSize:"1.2rem"}} />;
               else
                  item.solution = <CloseOutlined style={{color:"#f5222d", fontSize:"1.2rem"}}/>;
               item.key = key;
               return item;
            })
         }
      });
      setData(restData);
      setLoading(false);
      setUnchangedData(restData)
   }

   useEffect(() => {
      if (modules) {
          setLoading(true)
          get();
      }
  }, [])

  const filter = (e) => {
   const val = e.target.value.toLocaleLowerCase();
   const tempList = Object.assign([], unchangedData);
   if (val === '' || !val) {
      setData(unchangedData)
      return;
   }
   let filteredData = tempList.filter(t => isContainsFilterValue(t, val))
   setData(filteredData)
   setTotalCount(filteredData.length)
}

useEffect(() => {
   setTotalCount(data.length)
}, [data])

const isContainsFilterValue = (t, val) => {
   const isContains1 = !t.registeredUserId?  false : !t.registeredUserId.name?false:t.registeredUserId.name.toLowerCase().indexOf(val) !== -1;
   const isContains2 = !t.registeredUserId?  false : !t.registeredUserId.lastname?false:t.registeredUserId.lastname.toLowerCase().indexOf(val) !== -1;
   return isContains1 || isContains2;
}
 
let rowSelection = {
   onChange: (selectedRowKeys, selectedRows) => {
     setSelectedRows(selectedRows);
   },
   onSelect: (record, selected, selectedRows) => {
     setSelectedRows(selectedRows);
   },
   onSelectAll: (selected, selectedRows, changeRows) => {
     setSelectedRows(selectedRows);
   },
 };


   let deleteRow = async (item_id) => {
      api.delete(`/rest/requestAndComplaint/${item_id}`, ({data}) => {
      });
      let newData = data.filter(el => el._id !== item_id);
      setData(newData);
   }
   let deleteSelectedRows = async () => {
      selectedRows.map(item => {
         api.delete(`/rest/requestAndComplaint/${item._id}`, ({data}) => {
         });
         let newData = data.filter(el => el._id !== item._id);
         setData(newData);
         get();
      })
   }


   let columns = [
      {
         title: 'İşlem Numarası',
         dataIndex: 'transactionNumber',
         key: 'transactionNumber',
         sorter: (a, b) => a.transactionNumber - b.transactionNumber,
         sortDirections: ['descend', 'ascend']
      },
      {
         title: 'Ad',
         dataIndex: 'registeredUserId',
         key: 'registeredUserId',
         sorter: (a, b) => a.registeredUserId.name - b.registeredUserId.name,
         sortDirections: ['descend', 'ascend'],
         render: (registeredUserId) => registeredUserId && registeredUserId.name
      },
      {
         title: 'Soyad',
         dataIndex: 'registeredUserId',
         key: 'registeredUserId',
         sorter: (a, b) => a.registeredUserId.lastname - b.registeredUserId.lastname,
         sortDirections: ['descend', 'ascend'],
         render: (registeredUserId) => registeredUserId && registeredUserId.lastname
      },
      {
         title: 'SOZLESME_HESAP_NO',
         dataIndex: 'SOZLESME_HESAP_NO',
         key: 'SOZLESME_HESAP_NO',
         sorter: (a, b) => a.SOZLESME_HESAP_NO - b.SOZLESME_HESAP_NO,
         sortDirections: ['descend', 'ascend']
      },
      {
         title: 'Kategori Adı',
         dataIndex: 'categoryId',
         key: 'categoryId',
         sorter: (a, b) => a.categoryId.name - b.categoryId.name,
         sortDirections: ['descend', 'ascend'],
         render: (categoryId) => categoryId.name
      },
      {
         title: 'Çözüm Durumu',
         dataIndex: 'solution',
         key: 'solution',
         sorter: (a, b) => a.solution.value - b.solution.value,
         sortDirections: ['descend', 'ascend'],
         render: (solution) => solution
      },
      {
         title: 'Alt Kategori Adı',
         dataIndex: 'subCategoryId',
         key: 'subCategoryId',
         sorter: (a, b) => a.subCategoryId.name - b.subCategoryId.name,
         sortDirections: ['descend', 'ascend'],
         render: (subCategoryId) => subCategoryId.name
      },
      {
         title: 'Mesaj',
         dataIndex: 'message',
         key: 'message',
         sorter: (a, b) => a.message - b.message,
         sortDirections: ['descend', 'ascend']
      },
      {
         title: 'Action',
         key: 'action',
         className: 'editColumn',
         width: 150,
         render: (text, record) => (
            <Space size="small">
               <Link to={"/requestAndComplaint/detail/" + record._id}>
                  <Button icon={<FileTextOutlined/>}>
                     {!isSmall &&"Görüntüle"}
                  </Button>
               </Link>
               {/* <Link to={"/requestAndComplaint/edit/" + record._id}>
                        <Button icon={<EditOutlined />}>Düzenle</Button>
                    </Link>
                    <Popconfirm
                        onConfirm={() => deleteRow(record._id)} title="Silmeyi Onaylıyor musunuz?"
                        okText="Onayla" cancelText="Vazgeç">
                        <Button type="danger" icon={<DeleteOutlined />}>
                            Sil
                        </Button>
                    </Popconfirm> */}


            </Space>
         ),
      },
   ];
   let downloadExcel = async () => {
      var option = {};
      let dataTable = [];
      let query = []

      let excelData = await api.get(`/rest/requestAndComplaint?${qs.stringify({
         page: 1,
         pageSize: 10000,
         query
      })}`, {}).then(({data}) => {
         return data.result.rows;
      });

      if (excelData) {
         for (let i in excelData) {
            if (excelData) {
               let registeredUserId = excelData[i].registeredUserId
               let obj = {
                  'Ad': !!registeredUserId.name ? registeredUserId.name : " - ",
                  'Soyad': !!registeredUserId.lastname ? registeredUserId.lastname : " - ",
                  'TC': !!registeredUserId.tcNo ? registeredUserId.tcNo : " - ",
                  'Vergi No': !!registeredUserId.VERGI_NO ? registeredUserId.VERGI_NO : " - ",
                  'Sözleşme Hesap No': !!excelData[i].SOZLESME_HESAP_NO ? excelData[i].SOZLESME_HESAP_NO : " - ",
                  'Telefon': !!registeredUserId.phone ? registeredUserId.phone : " - ",
                  'Email': !!registeredUserId.email ? registeredUserId.email : " - ",
                  'Adres': !!excelData[i].address ? excelData[i].address : " - ",
                  'Mesaj': !!excelData[i].message ? excelData[i].message : " - ",
                  'Kategori': !!excelData[i].categoryId.name ? excelData[i].categoryId.name : " - ",
                  'Alt Kategori': !!excelData[i].subCategoryId.name ? excelData[i].subCategoryId.name : " - ",
                  'Oluşturulma Tarihi': excelData[i].createdAt ? moment(excelData[i].createdAt).format("DD-MM-YYYY") : '',
               }
               dataTable.push(obj);
            }
         }
      }

      let sheetAreas = ['Kategori', 'Sözleşme Hesap No', "Vergi No", "TC", 'Alt Kategori', 'Ad', 'Soyad', 'Telefon', 'Email', 'Adres', 'Mesaj', 'Oluşturulma Tarihi'];


      option.fileName = 'talepvesikayet'
      option.datas = [
         {
            sheetData: dataTable,
            sheetName: 'sheet',
            sheetFilter: ['Kategori', 'Sözleşme Hesap No', "Vergi No", "TC", 'Alt Kategori', 'Ad', 'Soyad', 'Telefon', 'Email', 'Adres', 'Mesaj', 'Oluşturulma Tarihi'],
            sheetHeader: ['Kategori', 'Sözleşme Hesap No', "Vergi No", "TC", 'Alt Kategori', 'Ad', 'Soyad', 'Telefon', 'Email', 'Adres', 'Mesaj', 'Oluşturulma Tarihi'],
            sheetFilter: sheetAreas,
            sheetHeader: sheetAreas,
         }
      ];

      var toExcel = new ExportJsonExcel(option);
      toExcel.saveExcel();
   }
   return (
      <div>
         <div className="list-head" style={{height:"250px !important"}}>
            <div className="list-title" style={{marginBottom:"30px !important"}}>
               <h1>{module ? module.name : ""}</h1>
            </div>
            <div className="list-buttons" style={{marginBottom:"30px !important"}}>
           
               <Link to="/requestAndComplaintCategories">
                  <Button type="light" icon={<AlignCenterOutlined/>} size="large"
                          style={{marginRight: "5px"}}>{!isSmall && 'Kategoriler'}</Button>
               </Link>
               <Button size="large" onClick={downloadExcel} style={{marginRight: "5px"}} icon={<DownloadOutlined/>}>
                  {!isSmall && 'Talep ve Şikayetleri İndir'}</Button>   
            </div>
         </div>
         <Alert size="large" className="alert-message_request-complaint" message="Kategorilere alt katagori eklemeyi unutmayınız!" banner/>
         <div className="table-wrap request">
            <Search placeholder="talep ve şikayet Ara" onChange={filter}/>
            <Table dataSource={data} columns={columns}
                   onChange={handleTableChange}
                   loading={{spinning: loading, indicator: <Loading3QuartersOutlined spin/>, size: "large"}}
                   pagination={{
                     total: totalCount
                  }}
                  rowSelection={{ ...rowSelection }}/>
                </div>
              </div>
            );
          };
          

export default RequestAndComplaint;
