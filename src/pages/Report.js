import React, {useGlobal} from 'reactn';
import api from '../service/index'
import {Row, Col, Button, Card,} from 'antd';
import qs from 'qs';
import ExportJsonExcel from 'js-export-excel';
import moment from 'moment';

const Report = (props) => {
    let [modules] = useGlobal('modules');
    let path = props.location.pathname.split('/')[1];
    let module;
    if (modules !== null) {
        [module] = modules.filter((el) => el._id === path);
    }
    let paymentReport = async () => {
        var option = {};
        let dataTable = [];
        let query = []

        let excelData = await api.get(`/rest/paymentLog?${qs.stringify({
            page: 1,
            pageSize: 10000,
            query
        })}`, {}).then(({data}) => {
            //data.result.rows = data.result.rows.filter(x => x.action !== "PaymentClick")
            return data.result.rows;
        });
        console.log(excelData)
        if (excelData) {
            for (let i in excelData) {
                if (excelData) {
                    let data = excelData[i]
                    if (data.bills) {
                        for (let j in excelData[i].bills) {
                            let bills = excelData[i].bills[j]
                            let obj = {
                                "İşlem ID": !!data.operationId ? data.operationId : " - ",
                                "Kullanıcı TC": !!data.tcNo ? data.tcNo : " - ",
                                "Vergi Numarası": !!data.vergiNo ? data.vergiNo : " - ",
                                "Email": !!data.email ? data.email : " - ",
                                "Telefon": !!data.phone ? data.phone : " - ",
                                "Kullanıcı Adı": !!data.name ? data.name : " - ",
                                "Kullanıcı Soyadı": !!data.lastname ? data.lastname : " - ",
                                "Sözleşme Hesap No": !!bills.SOZLESME_HESAP_NO ? bills.SOZLESME_HESAP_NO : " - ",
                                "Fatura No": !!bills.FATURA_NUMARASI ? bills.FATURA_NUMARASI : " - ",
                                'Tahsilat Sistemi': !!data.company ? data.company : " - ",
                                'Ödeme Sistemi': !!data.paymentCompany ? data.paymentCompany : " - ",
                                'Ücret': !!bills.TUTAR ? bills.TUTAR : " - ",
                                "Son Ödeme": !!bills.SON_ODEME ? bills.SON_ODEME : " - ",
                                'Ödeme Tarihi': !!data.updatedAt ? data.updatedAt : " - ",
                                'Ödeme Durumu': !!data.resultStatus ? data.resultStatus : bills.message,
                                'Banka Referans Numarası': !!bills.BankReferenceDocumentNumber ? bills.BankReferenceDocumentNumber : data.BankReferenceDocumentNumber
                            }
                            dataTable.push(obj);
                        }
                    }
                }
            }
        }
        let sheetAreas = ['İşlem ID', 'Kullanıcı TC', 'Vergi Numarası', 'Email', 'Telefon', 'Kullanıcı Adı', 'Kullanıcı Soyadı', 'Sözleşme Hesap No', 'Fatura No', 'Ödeme Sistemi', 'Tahsilat Sistemi', 'Ücret', 'Son Ödeme', 'Ödeme Tarihi', 'Ödeme Durumu', 'Ödeme Durumu', 'Banka Referans Numarası'];
        option.fileName = 'PaymentReport'
        option.datas = [
            {
                sheetData: dataTable,
                sheetName: 'sheet',
                sheetFilter: sheetAreas,
                sheetHeader: sheetAreas,
            }
        ];

        var toExcel = new ExportJsonExcel(option);
        toExcel.saveExcel();
    }
    let eBillReport = async () => {
        var option = {};
        let dataTable = [];
        let query = []

        let excelData = await api.get(`/rest/registeredUsers?${qs.stringify({
            page: 1,
            pageSize: 10000,
            query
        })}`, {}).then(({data}) => {
            return data.result.rows;
        });

        if (excelData) {
            console.log(excelData);
            for (let i in excelData) {
                if (excelData) {
                    let data = excelData[i]

                    let obj = {
                        'Ad': !!data.name ? data.name : " - ",
                        'Soyad': !!data.lastname ? data.lastname : " - ",
                        'Rol': !!data.role ? data.role : " - ",
                        'Telefon': !!data.phone ? data.phone : " - ",
                        'Email': !!data.email ? data.email : " - ",
                        "TC": !!data.tcNo ? data.tcNo : " - ",
                        "Vergi No": !!data.vkNo ? data.vkNo : " - ",
                    }
                    if (!data.email == "-" || !data.email == "") {
                        dataTable.push(obj);
                    }
                }
            }
        }
        let sheetAreas = ['Alt Kategori', 'Ad', 'Soyad', 'Telefon', 'Email', 'TC', 'Vergi No'];
        option.fileName = 'EFatura'
        option.datas = [
            {
                sheetData: dataTable,
                sheetName: 'sheet',
                sheetFilter: sheetAreas,
                sheetHeader: sheetAreas,
            }
        ];

        var toExcel = new ExportJsonExcel(option);
        toExcel.saveExcel();
    }
    let requestAndComplanintReport = async () => {
        var option = {};
        let dataTable = [];
        let query = []

        let excelData = await api.get(`/rest/requestAndComplaint?${qs.stringify({
            page: 1,
            pageSize: 10000,
            query
        })}`, {}).then(({data}) => {
            return data.result.rows;
        });

        if (excelData) {
            for (let i in excelData) {

                if (excelData) {
                    let registeredUserId = excelData[i].registeredUserId
                    let obj = {
                        'Kategori': excelData[i].categoryId && !!excelData[i].categoryId.name ? excelData[i].categoryId.name : " - ",
                        "Sözleşme Hesap No": registeredUserId && !!registeredUserId.SOZLESME_HESAP_NO ? registeredUserId.SOZLESME_HESAP_NO : " - ",
                        "Vergi No": registeredUserId && !!registeredUserId.VERGI_NO ? registeredUserId.VERGI_NO : " - ",
                        "TC": registeredUserId && !!registeredUserId.tcNo ? registeredUserId.tcNo : " - ",
                        'Alt Kategori': excelData[i].subCategoryId && !!excelData[i].subCategoryId.name ? excelData[i].subCategoryId.name : " - ",
                        'Ad': registeredUserId && !!registeredUserId.name ? registeredUserId.name : " - ",
                        'Soyad': registeredUserId && !!registeredUserId.lastname ? registeredUserId.lastname : " - ",
                        'Telefon': registeredUserId && !!registeredUserId.telNo ? registeredUserId.telNo : " - ",
                        'Email': registeredUserId && !!registeredUserId.email ? registeredUserId.email : " - ",
                        'Adres': !!excelData[i].address ? excelData[i].address : " - ",
                        'Mesaj': !!excelData[i].message ? excelData[i].message : " - ",
                        'Oluşturulma Tarihi': excelData[i].createdAt ? moment(excelData[i].createdAt).format("DD-MM-YYYY") : '',
                    }
                    dataTable.push(obj);

                }
            }
        }
        let sheetAreas = ['Kategori', 'Sözleşme Hesap No', 'Vergi No', 'TC', 'Alt Kategori', 'Ad', 'Soyad', 'Telefon', 'Email', "Adres", "Mesaj", "Oluşturulma Tarihi"];
        option.fileName = 'talepvesikayet'
        option.datas = [
            {
                sheetData: dataTable,
                sheetName: 'sheet',
                sheetFilter: sheetAreas,
                sheetHeader: sheetAreas,
            }
        ];

        var toExcel = new ExportJsonExcel(option);
        toExcel.saveExcel();
    }


    return (
        <div>
            <div className="list-head">
                <div className="list-title">
                    <h1>{module ? module.name : ''}</h1>
                </div>
            </div>
            <div className="form-wrap">
                <Card>
                    <Row direction="row" className="report-btn">
                        <Col xs={{span: 24}} md={{span: 6}}></Col>
                        <Col xs={{span: 24}} md={{span: 12}}>
                            <Button type="primary" size="large" onClick={paymentReport} block>
                                Ödeme raporları için tıklayınız.
                            </Button>
                        </Col>
                    </Row>
                    <Row direction="row" className="report-btn">
                        <Col xs={{span: 24}} md={{span: 6}}></Col>
                        <Col xs={{span: 24}} md={{span: 12}}>
                            <Button type="primary" size="large" onClick={requestAndComplanintReport} block>
                                Talep şikayet raporları için tıklayınız.
                            </Button>
                        </Col>
                    </Row>
                    <Row direction="row" className="report-btn">
                        <Col xs={{span: 24}} md={{span: 6}}></Col>
                        <Col xs={{span: 24}} md={{span: 12}}>
                            <Button type="primary" size="large" onClick={eBillReport} block>
                                E-faturaya geçiş raporları için tıklayınız.
                            </Button>
                        </Col>
                    </Row>
                    {/* <Row direction="row" className="report-btn">
                  <Col xs={{span: 24}} md={{span: 6}}></Col>
                  <Col xs={{span: 24}} md={{span: 12}}>
                     <Button type="primary" size="large" onClick={requestAndComplanintReport} block>
                        Yandex Metrica raporlarına erişim için tıklayınız.
                     </Button>
                  </Col>
               </Row> */}
                </Card>
            </div>
        </div>
    );
};

export default Report;