import React, {useEffect, useState, useGlobal} from "reactn";
import {useParams} from 'react-router-dom'
import {Row, Col, Form, Input, Button, Card, message, Select, Alert} from 'antd';
import {useHistory} from 'react-router-dom';
import {LeftOutlined} from "@ant-design/icons";
import {FileInput, MapInput} from '../components';

import api from '../service/index'
import {Link} from 'react-router-dom';
import {useCallback} from "react";

const CoordinateDetail = (props) => {

  let params = useParams()
  let history = useHistory()
  let id = params.id !== "add" ? params.id : false;
  let newRecord = {
    name:'',
    phone: '',
    area: '',
    color: "#1f8dff",
    images: [],
    content: '',
    urlCode: ""
  }
  let [isSmall, setIsSmall] = useGlobal('isSmall')

  let [data, setData] = useState(id ? [] : newRecord);
  let [errors, setErrors] = useState([]);
  let [loading, setLoading] = useState(id ? true : false);
  let [validationCheck, setValidationCheck] = useState(false)
  let areas = {"Adıyaman": "ADIYAMAN","Bölge dışı":"BÖLGE DIŞI","Elbistan": "ELBİSTAN","Kahramanmaraş": "KAHRAMANMARAŞ"};

  let [modules] = useGlobal("modules");
  let path = props.location.pathname.split('/')[1];
  let module
  if (modules !== null) {
    [module] = modules.filter(el => el._id === path);
  }

  //onetime run
  useEffect(() => {
    if (modules && id) {
      setLoading(true)
      api.get("/rest/coordinates/" + id).then(({data: {result, result_message}}) => {
        setData(result);
        setLoading(false)
      });
    }
  }, [id]);


  let validate = useCallback(() => {
    let errors = {};

    if (data.urlCode === null || data.urlCode.length === 0)
      errors.urlCode = 'Randevu Link Kodu Alanı Zorunludur!'

    if (data.name === null || data.name.length === 0)
      errors.name = 'Kurum Ad Alanı Zorunludur!'
  
   if (data.content  == null || data.content.length === 0)
       errors.content  = 'İçerik Alanı Zorunludur!'

   if (data.phone === null || data.phone.length === 0)
      errors.phone = 'Telefon Numarası Zorunludur!'

    if (data.phone == null || data.phone.length === 0)
        errors.phone = 'Zorunlu Alan!'

    errors.all = Object.getOwnPropertyNames(errors).map(n => errors[n]);
    errors.hasError = errors.all.length > 0;
    return errors;
  }, [data]);

  useEffect(() => {
    if (validationCheck) setErrors(validate())
  }, [validationCheck, data, validate]);


  let save = async () => {
    setValidationCheck(true)
    let err = validate()
    if (err.hasError) {
      setErrors(err)
      window.scrollTo({top: 20, behavior: 'smooth'});
    } else {
      if (id) {
        api.put("/rest/coordinates/" + id, data).then(({data: {result, result_message}}) => {
          if (result_message.type === 'success') {
            message.success("Bilgiler güncellendi", 2);
            history.push('/coordinate')
          } else
            message.error("Kayıt eklenirken bir hata oluştu.", 2);
        })
      } else {
        api.post("/rest/coordinates", data).then(({data: {result, result_message}}) => {
          if (result_message.type === 'success') {
            message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
            history.push('/coordinate')
          } else {
            message.error(result_message.message, 2);
          }
        })
      }
    }
  };

  return (
     <div>
       <div className="list-head">
         <div className="list-title">
           <h1>{module ? module.name : ""}</h1>
         </div>
         <div className="list-buttons">
           <Link to="/coordinate">
             <Button type="light" icon={<LeftOutlined/>} size="large">{!isSmall&&"GERİ"}</Button>
           </Link>
         </div>
       </div>
       <div className="form-wrap">
         <Card title={id ? "Düzenle" : "Ekle"} loading={loading}>
           <Form layout="horizontal" size={"large"} onFinish={save}>

             <Row direction="row">
               <Col xs={{span: 24}} md={{span: 24}}>
                 <Form.Item label="Kurum" required help={errors.name}
                            validateStatus={errors.name ? 'error' : 'success'}>
                   <Input name="name" value={data.name}
                          onChange={e => setData({...data, name: e.target.value})}/>
                 </Form.Item>
               </Col>
             </Row>
             <Row direction="row">
               <Col xs={{span: 24}} md={{span: 24}}>
                 <Form.Item label="Randevu Link  Kodu" required help={errors.urlCode}
                            validateStatus={errors.urlCode ? 'error' : 'success'}>
                   <Input name="urlCode" value={data.urlCode}
                          onChange={e => setData({...data, urlCode: e.target.value})}/>
                   <Alert message="Lütfen randevu link kodu alanını qsmart servislerindeki title alanını giriniz! Yanlış girildiği takdirde https://www.akedas.com.tr/randevu bu linke yönlendirilecektir." banner/>
                 </Form.Item>
               </Col>
             </Row>
             <Row direction="row">
               <Col xs={{span: 24}} md={{span: 24}}>
                 <Form.Item label="Bölge seçin :" required>
                   <Select value={areas[data.area]} mode='single' placeholder="Bölge seçin"
                           onChange={value => setData({...data, area: value})}>
                     {Object.keys(areas).map((areaKey, i) => (
                        <Select.Option value={areaKey} key={i}>{areas[areaKey]}</Select.Option>
                     ))}
                   </Select>
                 </Form.Item>
               </Col>
             </Row>
             <Row direction="row">
               <Col xs={{span: 24}} md={{span: 24}}>
                 <Form.Item label="Yetkili Kişi" required help={errors.authorizedPerson}
                            validateStatus={errors.authorizedPerson ? 'error' : 'success'}>
                   <Input name="authorizedPerson" value={data.authorizedPerson}
                          onChange={e => setData({...data, authorizedPerson: e.target.value})} />
                 </Form.Item>
               </Col>
             </Row>
             <Row direction="row">
               <Col xs={{span: 24}} md={{span: 24}}>
                 <Form.Item label="İçerik" required={true}>
                   <Input.TextArea onChange={e => setData({...data, content: e.target.value})}
                                   defaultValue={data.content}
                                   multiple={true} style={{width: "100%", height: 100}} value={data.content} required={true}/>
                 </Form.Item>
               </Col>
             </Row>
             <Row direction="row">
               {/*<Col xs={{ span: 24 }} md={{ span: 24 }}>*/}
               {/*  <Form.Item label="Kurum Kodu"required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>*/}
               {/*    <Input name="code" value={data.code} onChange={e => setData({ ...data, code: e.target.value })} />*/}
               {/*  </Form.Item>*/}
               {/*</Col>*/}
             </Row>
             <Row direction="row">
               {/*<Col xs={{ span: 24 }} md={{ span: 24 }}>*/}
               {/*  <Form.Item label="Şehir" required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>*/}
               {/*    <Input name="city" value={data.city} onChange={e => setData({ ...data, city: e.target.value })} />*/}
               {/*  </Form.Item>*/}
               {/*</Col>*/}
             </Row>
             <Row direction="row">
               {/*<Col xs={{ span: 24 }} md={{ span: 24 }}>*/}
               {/*  <Form.Item label="İlçe" required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>*/}
               {/*    <Input name="district" value={data.district} onChange={e => setData({ ...data, district: e.target.value })} />*/}
               {/*  </Form.Item>*/}
               {/*</Col>*/}
             </Row>
             <Row direction="row">
               {/*<Col xs={{ span: 24 }} md={{ span: 24 }}>*/}
               {/*  <Form.Item label="Mahalle/Köy" required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>*/}
               {/*    <Input name="locality" value={data.locality} onChange={e => setData({ ...data, locality: e.target.value })} />*/}
               {/*  </Form.Item>*/}
               {/*</Col>*/}
             </Row>
             <Row direction="row">
               <Col xs={{span: 24}} md={{span: 24}}>
                 <Form.Item label="Telefon" required help={errors.name}
                            validateStatus={errors.name ? 'error' : 'success'}>
                   <Input name="phone" value={data.phone}
                          onChange={e => setData({...data, phone: e.target.value})}  required={true}/>
                 </Form.Item>
               </Col>
             </Row>
             <Row direction="row">
                <Col span={24}>
                  <Form.Item label="Medya" required={(data.type === 'Image' || data.type === 'ImageContent' || data.type === 'SliderContent') ? true : false} help={errors.images} validateStatus={errors.images ? 'error' : 'success'}>
                    <FileInput name='images' title='Medyalar' type='file' accept="image/*" multi={true} ext={['image', 'video']} record={data} setRecord={setData} />
                    <Alert message="Yüklenecek görüntü 1200 x 834 çözünürlüğünde olmalıdır." banner />
                  </Form.Item>
                </Col>
              </Row>
             <Row direction="row">
               <Col span={24}>
                 <Form.Item label="Konum" required>
                   <MapInput record={data} setRecord={setData} name="coordinate" disabled={false}
                             title="coordinate" required={true}/>
                 </Form.Item>
               </Col>
             </Row>

             <Row direction="row">
               <Col span={24}>
                 <Form.Item>
                   <Button type="primary" disabled={loading} htmlType="submit" size="large"
                           block> KAYDET </Button>
                 </Form.Item>
               </Col>
             </Row>
           </Form>
         </Card>
       </div>
     </div>
  );
};


export default CoordinateDetail;
