import React, { useEffect, useState} from "react";
import { useGlobal } from "reactn";
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import api from './../service';
import Cookies from 'js-cookie';
import qs from 'qs';


const Login = (props) => {
  
    let [token, setToken] = useGlobal("token");
    let [modules, setModules] = useGlobal("modules");
    let [user, setUser] = useGlobal("user");
    let [settings, setSettings] = useGlobal("settings");
    let [langs, setLangs] = useGlobal("lang");
    let [logo, setLogo] = useGlobal("logo");

   const auth = async ({ username, password }) => {
      let params = {
        "email": username,
        "password": password,
        "loginType": "emailPass"
      }

      let query = {
        "lang" : "TR",
        "adminShow" : "true"
      }
     
      delete api.headers['Authorization'];
      await api.post('/api/login',params).then(async ({data}) => {  
 
        setToken(data.result.token);
        setUser(data.result);
        localStorage.setItem("user", JSON.stringify(data.result));
        //Cookies.set("user", data.result)
       
        api.setHeader('Authorization', "Bearer "+ data.result.token);
      
        Cookies.set("token", data.result.token)

        await api.get(`/rest/modules?${qs.stringify(query)}`).then(({data})=>{
          
          setModules(data.result.rows);
          localStorage.setItem("modules", JSON.stringify(data.result.rows));
          props.history.push(data.result.rows[0]._id);
        })

        await api.get(`/rest/settings?${qs.stringify()}`).then(({data})=>{
          setSettings(data.result.rows);
          if(data.result.rows){
            data.result.rows.forEach(element => {
              if(element._id === "customization"){
                setLogo(element.cmsLogo)
                localStorage.setItem("logo", JSON.stringify(element.cmsLogo));
              }
              if(element._id === "supportedLangs"){
                setLangs (element.items)
                localStorage.setItem("langs", JSON.stringify(element.items[0]));
              }
            });
          }
          localStorage.setItem("settings", JSON.stringify(data.result.rows));
        })
      
      }).catch((e) => {
        alert('Password or Username is incorrect');
      }); 

  };

  const onFinish = (values) => {
    auth(values);
  };

  return (
    <div className = "login-wrap">
    <div className="form">
      <Form
        name="normal_login"
        className="login-form"
        size="large"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <div className="logo"></div>
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your Username!',
            },
          ]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="E-Posta"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your Password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Sifre"
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="light"
            htmlType="submit"
            className="login-form-button login-btn"
            id="1"
          >
            Giriş yap
          </Button>
        </Form.Item>
      </Form>
    </div>
    </div>
  );
};

export default Login;
