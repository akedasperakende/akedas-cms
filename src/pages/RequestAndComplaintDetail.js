import React, {useEffect, useState, useGlobal} from "reactn";
import {useParams} from 'react-router-dom'
import {Row, Col, Form, Button, Card, message, Typography, Image, Input, Switch} from 'antd';
import {useHistory} from 'react-router-dom';
import {LeftOutlined,CheckOutlined,CloseOutlined} from "@ant-design/icons";
import api from '../service/index'
import {Link} from 'react-router-dom';

import {useCallback} from "react";

const RequestAndComplaintDetail = (props) => {

   const { TextArea } = Input;
   const {Paragraph, Text,} = Typography;
   let params = useParams()
   let history = useHistory()
   let id = params.id !== "add" ? params.id : false;
   let surveyId = params.surveyId !== "add" ? params.surveyId : false;
   let newRecord = {}
   let [data, setData] = useState(id ? {} : newRecord);
   let [errors, setErrors] = useState([]);
   let [loading, setLoading] = useState(id ? true : false);
   let [validationCheck, setValidationCheck] = useState(false)

   let [modules] = useGlobal("modules");
   let [pdfIcon] = useGlobal("pdfIcon");
   let [isSmall, setIsSmall] = useGlobal('isSmall')

   let path = props.location.pathname.split('/')[1];
   let module
   if (modules !== null) {
      [module] = modules.filter(el => el._id === path);
   }
  
   //onetime run
   useEffect(() => {
      if (modules && id) {
         setLoading(true)
         api.get("/rest/requestAndComplaint/" + id).then(({data: {result,result_message}}) => {
            setData(result);
            setLoading(false)
         });
      }
   }, [id]);


   let validate = useCallback(() => {
      let errors = {};
      return errors;
   }, [data]);

   useEffect(() => {
      if (validationCheck) setErrors(validate())
   },   [validationCheck, data, validate]);

   let save = async () => {
      setValidationCheck(true)
      let err = validate()
      if (err.hasError) {
         setErrors(err)
         window.scrollTo({top: 20, behavior: 'smooth'});
      } else {
         if (id) {

            api.put("/rest/requestAndComplaint/" + id, data).then(({data: {result, result_message}}) => {
               if (result_message.type === 'success') {
                  message.success("Bilgiler güncellendi", 2);
                  history.push('/requestAndComplaint')
               } else
                  message.error("Kayıt eklenirken bir hata oluştu.", 2);
            })
         } else {
            api.post("/rest/requestAndComplaint", data).then(({data: {result, result_message}}) => {
               if (result_message.type === 'success') {
                  message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
                  history.push('/requestAndComplaint')
               } else {
                  message.error(result_message.message, 2);
               }
            })
         }
      }
   };

   React.useEffect(() => {

   }, [data.questions])

   return (
      <div>
         <div className="list-head">
            <div className="list-title">
               <h1>{module ? module.name : ""}</h1>
            </div>
            <div className="list-buttons">
               <Link to={"/requestAndComplaint"}>
                  <Button type="light" icon={<LeftOutlined/>} size="large">{!isSmall&&"GERİ"}</Button>
               </Link>
            </div>
         </div>
         <div className="form-wrap">
            {!loading &&
            <Card title={id ? "" : ""} loading={loading}>
               <Form layout="horizontal" size={"large"} onFinish={save}>
                  <Row direction="row">
                     <Col xs={{span: 24}} sm={{span: 24}} md={{span: 10}} lg={{span: 10}} xl={{span: 6}}>
                        <Image width={200} src={data.registeredUserId.avatar.url}> </Image>
                     </Col>
                     <Col xs={{span: 24}} sm={{span: 24}} md={{span: 14}} lg={{span: 14}} xl={{span: 18}}>
                        <Col xs={{span: 24}} style={{ maxWidth: "70%", wordWrap: "break-word"}}>
                           <Typography.Title style={{padding: 0, margin: 0, whiteSpace: "normal"}}
                                             level={2}>{[data.registeredUserId.name, data.registeredUserId.lastname].filter(x => x).join(' ')}</Typography.Title>
                        </Col>
                        <Col xs={{span: 24}}>
                           <Typography.Text>Email
                              : {!!data.registeredUserId.email ? data.registeredUserId.email : " - "}</Typography.Text>
                        </Col>
                        <Col xs={{span: 24}}>
                           <Typography.Text>Telefon
                              : {!!data.registeredUserId.phone ? data.registeredUserId.phone : " - "}</Typography.Text>
                        </Col>
                        <Col xs={{span: 24}}>
                           <Typography.Text>TC
                              : {!!data.registeredUserId.tcNo ? data.registeredUserId.tcNo : " - "}</Typography.Text>
                        </Col>
                        <Col xs={{span: 24}}>
                           <Typography.Text>Vergi No
                              : {!!data.registeredUserId.VERGI_NO ? data.registeredUserId.VERGI_NO : " - "}</Typography.Text>
                        </Col>
                        <Col xs={{span: 24}}>
                           <Typography.Text>Sözleşme Hesap No
                              : {!!data.registeredUserId.SOZLESME_HESAP_NO ? data.registeredUserId.SOZLESME_HESAP_NO : " - "}</Typography.Text>
                        </Col>
                        <Col offset={16} xs={{ span: 8 }} md={{ span: 8 }}>
                           <Form.Item label="Durumu : " help={errors.solution} validateStatus={errors.solution ? 'error' : 'success'}>
                        <Switch checked={data.solution ? true : false} checkedChildren="Çözüldü" unCheckedChildren="Çözülmedi" onChange={v => setData({ ...data, solution: v })} />
                        </Form.Item>
                     </Col>
                        {/* <Col xs={{ span: 24 }}>
                    <Typography.Text>Firma : {data.registeredUserId.company}</Typography.Text>
                  </Col>
                  <Col xs={{ span: 24 }}>
                    <Typography.Text>Departman : {data.registeredUserId.department}</Typography.Text>
                  </Col>
                  <Col xs={{ span: 24 }}>
                    <Typography.Text>Pozisyon : {data.registeredUserId.position}</Typography.Text>
                  </Col> */}
                     </Col>
                  </Row>

                  <hr/>
                  <Row direction="row">
                     <Col xs={{span: 24}} md={{span: 24}}>
                        <Typography.Title style={{textAlign: "center"}}
                                          level={2}>{data.categoryId&& data.categoryId.name}</Typography.Title>
                     </Col>
                     <Col xs={{span: 24}} md={{span: 24}}>
                        <Typography.Title style={{textAlign: "left"}}
                                          level={4}>{data.subCategoryId.name}</Typography.Title>
                     </Col>
                  </Row>
                  <hr/>

                  <Row direction="row">
                     <Col xs={{span: 24}} md={{span: 24}}>
                        <Paragraph>
                           <pre>{data.message}</pre>
                        </Paragraph>
                     </Col>
                  </Row>
                  <Row direction="row">
                     <Col xs={{ span: 24 }} md={{ span: 24 }}>
                        <Form.Item label="Çözüm Mesajı : ">
                        <TextArea name="statusMessage" value={data.statusMessage} onChange={e => setData({ ...data, statusMessage: e.target.value })} />
                        </Form.Item>
                     </Col>
                  </Row>
                  <Row direction="row">
                     <Col span={24}>
                        <Form.Item>
                        <Button type="primary" disabled={loading} htmlType="submit" size="large" block > KAYDET </Button>
                        </Form.Item>
                     </Col>
                  </Row>
               </Form>
            </Card>
            }
         </div>
      </div>
   );
};


export default RequestAndComplaintDetail;
