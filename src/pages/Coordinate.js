import React, { useEffect, useState, useGlobal } from "reactn";
import { Table, Space, Button, Input,Popconfirm } from 'antd';
import api from '../service/index'
import { Link } from 'react-router-dom';
import qs from 'qs';

import { EditOutlined, DeleteOutlined, PlusOutlined, CheckOutlined, CloseOutlined, FileMarkdownOutlined, Loading3QuartersOutlined } from "@ant-design/icons";

const Coordinates = (props) => {

  const { Search } = Input;
  let [data, setData] = useState([]);
  let [totalCount, setTotalCount] = useState(0)
  let [selectedRows, setSelectedRows] = useState([])
  let [search, setSearch] = useState(false)
  let [loading, setLoading] = useState(false)
  let [isSmall, setIsSmall] = useGlobal('isSmall')

  let [modules] = useGlobal("modules");
  let path = props.location.pathname.split('/')[1];
  let module
  if(modules !== null){
    [module] = modules.filter(el => el._id === path);
  }

  let handleTableChange = async (page, filters, sorter) => {
    get(page, filters, sorter);
  }

  let get = async (page, filters, sorter) => {
    if (page) {
      page = { page: page.current, pageSize: page.pageSize }
    } else {
      page = { page: 1, pageSize: 10 };
    }
    let shortString = 'type';

    if (sorter) {
      if(Object.keys(sorter).length) {
        shortString = (sorter.order === 'descend' ? '-' : '') + sorter.field
      }
    }

    let _params = { sort: shortString, ...page, };

    if (search.length > 2) {
      _params["search"] = search.toLowerCase();
      _params["searchFields"] = "name,city,district,locality,phone,type";
    }
    let restData = await api.get(`/rest/coordinates?${qs.stringify(_params)}`, { _params }).then(({ data }) => {
      if (data.result) {
        setLoading(false)
        setTotalCount(data.result.total);
        return data.result.rows.map((item, key) => {
          if (item.active)
            item.active = <CheckOutlined />;
          else
            item.active = <CloseOutlined />;
          item.key = key;
          return item;
        })
      }
    });
    setData(restData);
  }

  useEffect(() => {
    if (modules) {
      setLoading(true)
      get();
    }
  }, [])

  useEffect(() => {
    get();
  }, [search]);

  let rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
      setSelectedRows(selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      setSelectedRows(selectedRows);
    },
  };


  let deleteRow = async (item_id) => {
    api.delete(`/rest/coordinates/${item_id}`, ({ data }) => { });
    let newData = data.filter(el => el._id !== item_id);
    setData(newData);
  }
  let deleteSelectedRows = async () => {
    selectedRows.map(item => {
      api.delete(`/rest/coordinates/${item._id}`, ({ data }) => { });
      let newData = data.filter(el => el._id !== item._id);
      setData(newData);
      get();
    })
  }

  let getReport = async () => {
    let result = await api.get(`/rest/reports/coordinates`)
    const file = new Blob(["\ufeff", result.data]);
    let _url = window.URL.createObjectURL(file);
    let a = document.createElement('a');
    a.href = _url;
    a.download = `Hizmet_Noktaları_Raporu.xls`;
    document.body.appendChild(a);
    a.click();
    a.remove();
  }

  let onChange = async (e) => {
    setSearch(e.target.value);
    get();
  }

  let columns = [
    {
      title: 'Kurum',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => a.name - b.name,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Kurum Kodu',
      dataIndex: 'urlCode',
      key: 'urlCode',
      sorter: (a, b) => a.urlCode - b.urlCode,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Bölge',
      dataIndex: 'area',
      key: 'area',
      sorter: (a, b) => a.area - b.area,
      sortDirections: ['descend', 'ascend']
    },
    // {
    //   title: 'İlçe',
    //   dataIndex: 'district',
    //   key: 'district',
    //   sorter: (a, b) => a.district - b.district,
    //   sortDirections: ['descend', 'ascend']
    // },
    // {
    //   title: 'Mahalle/Köy',
    //   dataIndex: 'locality',
    //   key: 'locality',
    //   sorter: (a, b) => a.locality - b.locality,
    //   sortDirections: ['descend', 'ascend']
    // },
    {
      title: 'Telefon',
      dataIndex: 'phone',
      key: 'phone',
      sorter: (a, b) => a.phone - b.phone,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Action',
      key: 'action',
      className: 'editColumn',
      width: 150,
      render: (text, record) => (
         <Space size="small">
           <Link to={"/coordinate/edit/" + record._id}>
             <Button icon={<EditOutlined />}>
              {!isSmall &&" Düzenle"}
             </Button>
           </Link>
           <Popconfirm
              onConfirm={() => deleteRow(record._id)} title="Silmeyi Onaylıyor musunuz?"
              okText="Onayla" cancelText="Vazgeç">
             <Button type="danger" icon={<DeleteOutlined />}>
               {!isSmall&&"Sil"}
             </Button>
           </Popconfirm>
         </Space>
      ),
    },
  ];

  return (
     <div>
       <div className="list-head">
         <div className="list-title">
           <h1>{module ? module.name : ""}</h1>
         </div>
         <div className="list-buttons">
           <Button type="primary" icon={<FileMarkdownOutlined />} size="large" style={{ marginRight: "5px" }} onClick={() => getReport()}>{!isSmall && "Dışa Aktar" }</Button>
           <Button type="danger" icon={<DeleteOutlined />} size="large" onClick={deleteSelectedRows} style={{ marginRight: "5px" }}>{!isSmall && "Seçilenleri Sil"}</Button>
           <Link to="/coordinate/add">
             <Button type="light" icon={<PlusOutlined />} size="large">{!isSmall && "Yeni Ekle"}</Button>
           </Link>
         </div>
       </div>

       <div className="table-wrap">
         <Search type="light" placeholder="Kurum-Şehir-İlçe-Mahalle-Telefon-Tip Ara" onChange={onChange} onSearch={(v) => { setSearch(v); get() }} />
         <Table dataSource={data} columns={columns} loading={{ spinning: loading,indicator:<Loading3QuartersOutlined spin /> ,size:"large"}}
                onChange={handleTableChange}
                pagination={{
                  total: totalCount
                }}
                rowSelection={{ ...rowSelection }} />
       </div>
     </div>
  );
};



export default Coordinates;
