import React, { useEffect, useState, useGlobal } from "reactn";
import { useParams } from 'react-router-dom'
import {Table,Space,Button,Popconfirm,message} from 'antd';
import api from '../service/index'
import { Link } from 'react-router-dom';
import qs from 'qs';

import {EditOutlined, DeleteOutlined,PlusOutlined, CheckOutlined,FileExcelOutlined,CloseOutlined, LeftOutlined, Loading3QuartersOutlined} from "@ant-design/icons";

const RequestAndComplaintSubCategoriesList = (props) => {
  let params = useParams()
  let query=[]
  let requestAndComplaintSubCategories=params.id !== "add" ? params.id : false;
  query["categoryId"]  =requestAndComplaintSubCategories;
  let [data, setData] = useState([]);
  let [totalCount, setTotalCount] = useState(0)
  let [selectedRows, setSelectedRows] = useState([])
  let [loading, setLoading] = useState(false)
  let [isSmall, setIsSmall] = useGlobal('isSmall')

  let [modules] = useGlobal("modules");
  let path = props.location.pathname.split('/')[1];
  let module
  if(modules !== null){
     [module] = modules.filter(el => el._id === path);
  }

  let handleTableChange = async (page, filters, sorter) => {
    get(page,filters,sorter);
  }

  let get = async (page, filters, sorter) => {
    if(page) {
      page =  { page : page.current, pageSize: page.pageSize }
    } else {
      page = { page: 1, pageSize: 10 };
    }
    let shortString = 'order';

    if (sorter) {
      if(Object.keys(sorter).length) {
          shortString = (sorter.order === 'descend' ? '-' : '') + sorter.field
      }
    }

    let _params = {sort: shortString, ...page, query};
   
    let restData = await api.get(`/rest/requestAndComplaintSubCategories?${qs.stringify(_params)}`, {_params}).then(({data}) => {
      setLoading(false)
      setTotalCount(data.result.total);
        return data.result.rows.map((item,key)=>{
          if (item.active === "1") 
          item.active = <CheckOutlined />;
          else 
          item.active = <CloseOutlined />;
          item.key = key;
          return item;
        })
      });
    setData(restData);
  }

  useEffect(() => {
      if (modules) {
          setLoading(true)
          get();
      }
  }, [])
 
  let rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
      setSelectedRows(selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      setSelectedRows(selectedRows);
    },
  };


  let deleteRow = async (item_id) => {
      api.delete(`/rest/requestAndComplaintSubCategories/${item_id}`).then((
        {data: { result, result_message }}) => {
        if (result_message.type == "error") message.error(result_message.message)
        else {
          let newData = data.filter(el => el._id !== item_id);
          setData(newData);
        }
      });
  }
  let deleteSelectedRows = async () => {
    selectedRows.map(item => {
      api.delete(`/rest/requestAndComplaintSubCategories/${item._id}`, ({data}) => {});
      let newData = data.filter(el => el._id !== item._id);
      setData(newData);
      get();
    })
  }

  let getReport = async (record) => {
    let result = await api.get(`/rest/reports/requestAndComplaintSubCategories?id=${record._id}`)
    const file = new Blob(["\ufeff", result.data]);
    let _url = window.URL.createObjectURL(file);
    let a = document.createElement('a');
    a.href = _url;
    a.download = `${record.name.replace(/ /g, '')}_report.xls`;
    document.body.appendChild(a);
    a.click();
    a.remove();
  }
  let columns = [
    {
      title: 'Başlık',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => a.name - b.name,
      sortDirections: ['descend', 'ascend']
  },
  {
      title: 'Aktif',
      dataIndex: 'active',
      key: 'active',
      sorter: (a, b) => a.active - b.active,
      sortDirections: ['descend', 'ascend']
  },
    {
      title: 'Action',
      key: 'action',
      className: 'editColumn',
      width: 150,
      render: (text, record) => (
        <Space size="small">
          <Button type="primary"  icon={<FileExcelOutlined />} onClick={() => getReport(record)}>{!isSmall&&"Rapor"}</Button>
           <Popconfirm
            onConfirm={() => deleteRow(record._id)} title="Silmeyi Onaylıyor musunuz?"
            okText="Onayla" cancelText="Vazgeç">
            <Button type="danger" icon={<DeleteOutlined />}>
              {!isSmall&&"Sil"}
            </Button>
          </Popconfirm>
          <Link to={"/requestAndComplaintCategories/detail/" + requestAndComplaintSubCategories + "/edit/" + record._id}>
            <Button icon={<EditOutlined />}>{!isSmall&&"Düzenle"}</Button>
          </Link>
        </Space>
      ),
    },
  ];

  return (
    <div> 
      <div className="list-head">
        <div className="list-title">
           <h1>{module ? module.name : ""}</h1> 
        </div>
        <div className="list-buttons">
          
          {!isSmall && <Button type="danger" icon={<DeleteOutlined />} size = "large" onClick={deleteSelectedRows} style={{ marginRight:"5px" }}>Seçilenleri Sil</Button>}
          <Link to={"/requestAndComplaintCategories/detail/"+requestAndComplaintSubCategories+"/add"}>
            <Button type="light" icon={<PlusOutlined />} size = "large" style={{ marginRight:"5px" }}>Yeni Ekle</Button>
          </Link>
          <Link to="/requestAndComplaintCategories">
            <Button type="light" icon={<LeftOutlined />} size="large" style={{ marginRight:"5px" }} >GERİ</Button>
          </Link>
        </div>
      </div>

      <div className="table-wrap">
        <Table dataSource={data} columns={columns}
        onChange={handleTableChange} loading={{ spinning: loading,indicator:<Loading3QuartersOutlined spin /> ,size:"large"}}
        pagination={{
           total: totalCount
        }}
        rowSelection={{ ...rowSelection }}/>
      </div>
      
    </div>
  );
};



export default RequestAndComplaintSubCategoriesList;
