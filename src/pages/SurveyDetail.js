import React, { useEffect, useState, useGlobal } from "reactn";
import { useParams } from 'react-router-dom'
import { Row, Col, Form, Input, Button, Switch, Card, message,Alert } from 'antd';
import { useHistory } from 'react-router-dom';
import { LeftOutlined } from "@ant-design/icons";
import {  QuestionsInput} from '../components';

import api from '../service/index'
import { Link } from 'react-router-dom';

import { useCallback } from "react";

const SurveyDetail = (props) => {

  let params = useParams()
  let history = useHistory()
  let id = params.id !== "add" ? params.id : false;
  let [langs] = useGlobal("langs")

  let newRecord = {
    lang: langs ? langs.code : "TR",
    name: '',
    groups:[],
    sendNotification:false
  }

  let [data, setData] = useState(id ? {} : newRecord);
  let [answers, setAnswers] = useState();
  let [errors, setErrors] = useState([]);
  let [loading, setLoading] = useState(id ? true : false);
  let [validationCheck, setValidationCheck] = useState(false)
  let [isSmall, setIsSmall] = useGlobal('isSmall')

  let [modules] = useGlobal("modules");
  let path = props.location.pathname.split('/')[1];
  let module
  if(modules !== null){
     [module] = modules.filter(el => el._id === path);
  }

  //onetime run
  useEffect(() => {
    if(modules && id){
      setLoading(true)
        api.get("/rest/surveys/" + id).then(({ data: { result, result_message } }) => {
          setData(result);
          setLoading(false)
        });


      api.get("/rest/survey/countAnswers/" + id).then(({ data: { result, result_message } }) => {
        setAnswers(result);
        setLoading(false)
      });
    }
  }, [id]);

  let validate = useCallback(() => {
    let errors = {};

    if (data.lang == null)
      errors.lang = 'Zorunlu Alan!'

    if (data.name == null || data.name.length === 0)
      errors.name = 'Zorunlu Alan!'

    if (data.questions == null)
      errors.questions = 'Zorunlu Alan!'


    errors.all = Object.getOwnPropertyNames(errors).map(n => errors[n]);
    errors.hasError = errors.all.length > 0;
    return errors;
  }, [data]);

  useEffect(() => { if (validationCheck) setErrors(validate()) }, [validationCheck, data, validate]);

  let save = async () => {
    setValidationCheck(true)
    let err = validate()
    if (err.hasError) {
      setErrors(err)
      window.scrollTo({ top: 20, behavior: 'smooth' });
    }
    else {
      if (id) {

        api.put("/rest/surveys/" + id, data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Bilgiler güncellendi", 2);
            history.push('/survey')
          }
          else
            message.error("Kayıt eklenirken bir hata oluştu.", 2);
        })
      } else {
        api.post("/rest/surveys", data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
            history.push('/survey')
          } else {
            message.error(result_message.message, 2);
          }
        })
      }
    }
  };

  return (
    <div>
      <div className="list-head">
        <div className="list-title">
          <h1>{module ? module.name : ""}</h1>
        </div>
        <div className="list-buttons">
          <Link to="/survey">
            <Button type="light" icon={<LeftOutlined />} size="large">{!isSmall &&"GERİ"}</Button>
          </Link>
        </div>
      </div>
      <div className="form-wrap">
          <Card title={id ? "Düzenle" : "Ekle"}loading={loading}>
            <Form layout="horizontal" size={"large"} onFinish={save}>
              {/* <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Dil">
                    <LangPicker record={data} setRecord={setData} name="lang" />
                  </Form.Item>
                </Col>
              </Row> */}

              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 24 }}>
                  <Form.Item label="Başlık" required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                    <Input name="name" value={data.name} onChange={e => setData({ ...data, name: e.target.value })} />
                  </Form.Item>
                </Col>

              </Row>

              <Row direction="row">
                {/* <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Bildirim" help={errors.sendNotification} validateStatus={errors.sendNotification ? 'error' : 'success'}>
                    <SendNotificationInput record={data} setRecord={setData} name="sendNotification" />
                  </Form.Item>
                </Col> */}
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Durumu" help={errors.active} validateStatus={errors.active ? 'error' : 'success'}>
                    <Switch checked={!!data.active} checkedChildren="Aktif" unCheckedChildren="Pasif" onChange={v => setData({ ...data, active: v })} />
                  </Form.Item>
                </Col>
              </Row>

              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 24 }}>
                  <Form.Item label="Sorular" required help={errors.questions} validateStatus={errors.questions ? 'error' : 'success'}>
                  {answers>0&&
                    <Alert type="info" message="Bu ankete en az bir cevap verildiği için düzenleyemezsiniz!" banner />
                  }
                    <QuestionsInput name='questions' record={data} setRecord={setData}  disabled={answers > 0}  />
                  </Form.Item>
                </Col>
              </Row>

              <Row direction="row">
                <Col span={24}>
                  <Form.Item>
                    <Button type="primary" disabled={loading} htmlType="submit" size="large" block > KAYDET </Button>
                  </Form.Item>
                </Col>
              </Row>

            </Form>
          </Card>
      </div>
    </div>
  );
};



export default SurveyDetail;
