import React, {useEffect, useState, useGlobal} from "reactn";
import {Table, Space, Button, Input, Popconfirm, message} from 'antd';
import ExportJsonExcel from 'js-export-excel';
import moment from 'moment';
import api from '../service/index'
import qs from 'qs';

import {
   DeleteOutlined,
   CheckOutlined,
   CloseOutlined,
   DownloadOutlined,
   Loading3QuartersOutlined
} from "@ant-design/icons";

const User = (props) => {
   const {Search} = Input;
   let [data, setData] = useState([]);
   let [excelData, setExcelData] = useState([]);
   let [totalCount, setTotalCount] = useState(0)
   let [activeUserCount, setActiveUserCount] = useState(0)
   let [loginUserCount, setLoginUserCount] = useState(0)
   let [adminCount, setAdminCount] = useState(0)
   let [selectedRows, setSelectedRows] = useState([])
   let [loading, setLoading] = useState(false);
   let [unchangedData,setUnchangedData]=useState([])
   let [isSmall, setIsSmall] = useGlobal('isSmall')

   let [modules] = useGlobal("modules");
   let path = props.location.pathname.split('/')[1];
   let [module] = modules.filter(el => el._id === path);

   let query = []
   query["deleted"] = false;
   query["allow_donation"] = false;
   query["blood_donation"] = false;
   query["platelet_donation"] = false;
   let handleTableChange = async (page, filters, sorter) => {
      get(page, filters, sorter);
   }

   let profileSettings;
   let customization;
   let settings = useGlobal("settings")[0]
   if (settings) {
      settings.forEach(element => {
         if (element._id === "profileSettings") {
            profileSettings = element
         }
         if (element._id === "customization") {
            customization = element
         }
      });
   }

   let get = async (page, filters, sorter) => {
      page={page:1,pageSize:Number.MAX_VALUE}
      let shortString = 'name';
      setLoading(true)
      if (sorter) {
         if (Object.keys(sorter).length) {
            shortString = (sorter.order === 'descend' ? '-' : '') + sorter.field
         }
      }

      let _params = {sort: shortString, ...page, query};

      let restData = await api.get(`/rest/registeredUsers?${qs.stringify(_params)}`, {_params}).then(({data}) => {
         if (data.result) {
            setLoading(false)
            setTotalCount(data.result.total);
            return data.result.rows.map((item, key) => {
               if(item.vkNo==null){
                  item.vkNo='';
               }
               if(item.tcNo==null){
                  item.tcNo='';
               }
               if (item.active)
                  item.active = <CheckOutlined/>;
               else
                  item.active = <CloseOutlined/>;
               if (!item.hasOwnProperty('groups')) {
                  item.groups = []
               }
               if (item.isLogin)
                  item.isLogin = <div className="true-circle"></div>;
               else
                  item.isLogin = <div className="false-circle"></div>;

               item.key = key;
               return item;
            })
         }
      });
      setData(restData);
      setLoading(false);
      setUnchangedData(restData)
   }
   let activeCount = () => {
      api.get(`/api/usersActiveCount?isLogin=true`, {}).then(({data: {result, result_message}}) => {
         if(result.activeUserCount){
            result.activeUserCount==null ? setActiveUserCount(0) : setActiveUserCount(result.activeUserCount)
         }
         result.loginUserCount==null ? setLoginUserCount(0) : setLoginUserCount(result.loginUserCount)
         result.adminCount==null ? setAdminCount(0) : setAdminCount(result.adminCount)
      });

   }
   useEffect(() => {
      if (modules) {
         setLoading(true)
         activeCount()
         get();
      }
   }, [])

//    let rowSelection = {
//       onChange: (selectedRowKeys, selectedRows) => {
//         setSelectedRows(selectedRows);
//       },
//       onSelect: (record, selected, selectedRows) => {
//         setSelectedRows(selectedRows);
//       },
//       onSelectAll: (selected, selectedRows, changeRows) => {
//         setSelectedRows(selectedRows);
//       },
//     };
  
//     let deleteRow = async (item_id) => {
//       api.delete(`/rest/registeredUsers/${item_id}`, ({ data }) => { });
//       let newData = data.filter(el => el._id !== item_id);
//       setData(newData);
//       // await activeCount()
//   }
     
   let deleteSelectedRows = async () => {
      await selectedRows.map(item => {
         api.delete(`/rest/registeredUsers/${item._id}`, ({data}) => {
         });
         let newData = data.filter(el => el._id !== item._id);
         setData(newData);
         get();
      })
     await window.location.reload(false);
   }


   let getReport = async () => {
      let result = await api.get(`/rest/reports/loggedInUsers`)
      const file = new Blob(["\ufeff", result.data]);
      let _url = window.URL.createObjectURL(file);
      let a = document.createElement('a');
      a.href = _url;
      a.download = `Kullanici_Giriş_Raporu.xls`;
      document.body.appendChild(a);
      a.click();
      a.remove();
   }

   const filter = (e) => {
      const val = e.target.value.toLocaleLowerCase();
      const tempList = Object.assign([], unchangedData);
      if (val === '' || !val) {
       setData(unchangedData)
         return;
      }
      let filteredData = tempList.filter(t => isContainsFilterValue(t, val))
      setData(filteredData)
      setTotalCount(filteredData.length)
   }

   useEffect(() => {
      setTotalCount(data.length)
   }, [data])


   const isContainsFilterValue = (t, val) => {
      const isContains1 = t.tcNo == null ? false : t.tcNo.toLowerCase().indexOf(val) !== -1;
      const isContains2 = t.telNo == null ? false : t.telNo.toLowerCase().indexOf(val) !== -1;
      const isContains3 = t.lastname == null ? false : t.lastname.toLowerCase().indexOf(val) !== -1;
      const isContains4 = t.name == null ? false : t.name.toLowerCase().indexOf(val) !== -1;
      const isContains5 = t.email == null ? false : t.email.toLowerCase().indexOf(val) !== -1;
      return isContains1 || isContains2 || isContains3 || isContains4||isContains5;
   }
   let columns = [
      {
         title: 'Email',
         dataIndex: 'email',
         key: 'email',
         sorter: (a, b) => a.email - b.email,
         sortDirections: ['descend', 'ascend']
      }, {
         title: 'Telefon',
         dataIndex: 'phone',
         key: 'phone',
         sorter: (a, b) => a.phone - b.phone,
         sortDirections: ['descend', 'ascend']
      }, {
         title: 'İsim',
         dataIndex: 'name',
         key: 'name',
         sorter: (a, b) => a.name - b.name,
         sortDirections: ['descend', 'ascend']
      }, {
         title: 'Soyisim',
         dataIndex: 'lastname',
         key: 'lastname',
         sorter: (a, b) => a.lastname - b.lastname,
         sortDirections: ['descend', 'ascend']
      },
      {
         title: 'TC Numarası',
         dataIndex: 'tcNo',
         key: 'tcNo',
         sorter: (a, b) => a.tcNo - b.tcNo,
         sortDirections: ['descend', 'ascend']
      },
      ,
      {
         title: 'Vergi Numarası',
         dataIndex: 'vkNo',
         key: 'vkNo',
         sorter: (a, b) => a.vkNo - b.vkNo,
         sortDirections: ['descend', 'ascend']
      },
      // {
      //    title: 'Grup',
      //    dataIndex: 'groups',
      //    key: 'groups',
      //    sorter: (a, b) => a.groups - b.groups,
      //    sortDirections: ['descend', 'ascend'],
      //    render: (groups) => groups.length > 0 && groups.map(e => <div style={{marginBottom: 5}}>{e.name}</div>)
      // },
      {
         title: '-',
         width: "50px",
         dataIndex: 'isLogin',
         key: 'isLogin',
         sorter: (a, b) => a.isLogin - b.isLogin,
         sortDirections: ['descend', 'ascend']
      },
      // {
      //    title: 'Action',
      //    key: 'action',
      //    className: 'editColumn',
      //    width: 150,
      //    render: (text, record) => (
      //       <Space size="small">
      //          {/* <Link to={"/registeredUsers/edit/" + record._id}>
      //       <Button icon={<EditOutlined />}>
      //         Düzenle
      //       </Button>
      //     </Link> */}
      //          <Popconfirm
      //                   onConfirm={() => deleteRow(record._id)} title="Silmeyi Onaylıyor musunuz?"
      //                   okText="Onayla" cancelText="Vazgeç">
      //                   <Button type="danger" icon={<DeleteOutlined />}>
      //                       Sil
      //                   </Button>
      //               </Popconfirm>
      //       </Space>
      //    ),
      // },
   ];

   let totalOnline = async () => {

      let restData = await api.get(`/rest/registeredUsers?${qs.stringify({
         page: 1,
         pageSize: 10000,
         query
      })}`, {}).then(({data}) => {
         console.log(data)
         return data.result.rows;
      });

   }


   let downloadExcel = async () => {
      var option = {};
      let dataTable = [];
      let query = []
      if (customization.isSmarteventView) {
         query["deleted"] = false;
      }

      let excelData = await api.get(`/rest/registeredUsers?${qs.stringify({
         page: 1,
         pageSize: 10000,
         query
      })}`, {}).then(({data}) => {
         return data.result.rows;
      });

      if (excelData) {
         console.log(excelData)
         for (let i in excelData) {
            if (excelData) {
               let data = excelData[i]
               let obj = {
                  'Ad': !!data.name ? data.name : " - ",
                  'Soyad': !!data.lastname ? data.lastname : " - ",
                  'Rol': !!data.role ? data.role : " - ",
                  'Telefon': !!data.phone ? data.phone : " - ",
                  'Email': !!data.email ? data.email : " - ",
                  "TC": !!data.tcNo ? data.tcNo : " - ",
                  "Vergi No": !!data.vkNo ? data.vkNo : " - ",
                  'Silinmiş Mi': excelData[i].deleted ? 'Silindi' : 'Aktif',
                  'Giriş Durumu': excelData[i].isLogin ? 'Giriş Yaptı' : 'Giriş Yapmadı',
               }
               dataTable.push(obj);
            }
         }
      }

      let sheetAreas = ['Ad', 'Soyad', 'Rol', 'Telefon', 'Email', 'TC', 'Vergi No', 'Silinmiş Mi', 'Giriş Durumu'];
      //smartevent için
      if (customization.isSmarteventView) {
         sheetAreas = ['Ad', 'Soyad', 'Rol', 'Telefon', 'Email', 'TC', 'Vergi No', 'Silinmiş Mi', 'Giriş Durumu'];
      }
      option.fileName = 'UserList'
      option.datas = [
         {
            sheetData: dataTable,
            sheetName: 'sheet',
            sheetFilter: ['Ad', 'Soyad', 'Rol', 'Telefon', 'Email', 'TC', 'Vergi No', 'Silinmiş Mi', 'Giriş Durumu'],
            sheetHeader: ['Ad', 'Soyad', 'Rol', 'Telefon', 'Email', 'TC', 'Vergi No', 'Silinmiş Mi', 'Giriş Durumu'],
            sheetFilter: sheetAreas,
            sheetHeader: sheetAreas,
         }
      ];

      var toExcel = new ExportJsonExcel(option);
      toExcel.saveExcel();
   }
   let parseDateExcel = (excelTimestamp) => {
      const secondsInDay = 24 * 60 * 60;
      const excelEpoch = new Date(1899, 11, 31);
      const excelEpochAsUnixTimestamp = excelEpoch.getTime();
      const missingLeapYearDay = secondsInDay * 1000;
      const delta = excelEpochAsUnixTimestamp - missingLeapYearDay;
      const excelTimestampAsUnixTimestamp = excelTimestamp * secondsInDay * 1000;
      const parsed = excelTimestampAsUnixTimestamp + delta;
      return isNaN(parsed) ? null : parsed;
   };
   let uploadExcel = async () => {

      let user_id;
      let userData;
      let userGroups = [];

      let updateCount = 0;
      let insertCount = 0;

      if (excelData) {

         for (let i in excelData) {
            user_id = excelData[i].id;
            userGroups = [];
            if (excelData[i].group_1) {
               userGroups.push(excelData[i].group_1.toString().trim())
            }
            if (excelData[i].group_2) {
               userGroups.push(excelData[i].group_2.toString().trim())
            }
            if (excelData[i].group_3) {
               userGroups.push(excelData[i].group_3.toString().trim())
            }
            if (excelData[i].group_4) {
               userGroups.push(excelData[i].group_4.toString().trim())
            }
            if (excelData[i].group_5) {
               userGroups.push(excelData[i].group_5.toString().trim())
            }
            if (excelData[i].group_6) {
               userGroups.push(excelData[i].group_6.toString().trim())
            }
            if (excelData[i].group_7) {
               userGroups.push(excelData[i].group_7.toString().trim())
            }
            if (excelData[i].group_8) {
               userGroups.push(excelData[i].group_8.toString().trim())
            }
            if (excelData[i].group_9) {
               userGroups.push(excelData[i].group_9.toString().trim())
            }
            if (excelData[i].group_10) {
               userGroups.push(excelData[i].group_10.toString().trim())
            }
            if (excelData[i].group_11) {
               userGroups.push(excelData[i].group_11.toString().trim())
            }
            if (excelData[i].group_12) {
               userGroups.push(excelData[i].group_12.toString().trim())
            }
            if (excelData[i].group_13) {
               userGroups.push(excelData[i].group_13.toString().trim())
            }
            if (excelData[i].group_14) {
               userGroups.push(excelData[i].group_14.toString().trim())
            }
            if (excelData[i].group_15) {
               userGroups.push(excelData[i].group_15.toString().trim())
            }
            if (excelData[i].group_16) {
               userGroups.push(excelData[i].group_16.toString().trim())
            }
            if (excelData[i].group_17) {
               userGroups.push(excelData[i].group_17.toString().trim())
            }
            if (excelData[i].group_18) {
               userGroups.push(excelData[i].group_18.toString().trim())
            }
            if (excelData[i].group_19) {
               userGroups.push(excelData[i].group_19.toString().trim())
            }
            if (excelData[i].group_20) {
               userGroups.push(excelData[i].group_20.toString().trim())
            }
            if (typeof excelData[i].birthday === 'number') {
               excelData[i].birthday = moment(parseDateExcel(excelData[i].birthday)).add(12, 'hours').format()
            }

            userData = {
               "name": excelData[i].name,
               "lastname": excelData[i].lastname || '',
               "email": excelData[i].email || '',
               "phone": excelData[i].phone || '',
               "location": excelData[i].city || '',
               "birthday": excelData[i].birthday || '',
               "groups": userGroups,
            }
            //Ek alanlar
            if (profileSettings.showCompany) {
               userData.company = excelData[i].company.toString().trim() || '';
            }
            if (profileSettings.showPosition) {
               userData.position = excelData[i].position.toString().trim() || '';
            }
            if (profileSettings.showDepartment) {
               userData.department = excelData[i].department.toString().trim() || '';
            }

            if (user_id) {
               // update
               await api.put("/rest/registeredUsers/" + user_id, userData).then(({data: {result, result_message}}) => {
                  console.log(result_message);
               })
               updateCount++;
            } else {
               // insert
               userData["role"] = "user";
               userData["password"] = "passVeli";

               await api.post("/rest/registeredUsers", userData).then(({data: {result, result_message}}) => {
                  console.log(result_message);
               })
               insertCount++;
            }


         }
         if (updateCount || insertCount) {
            message.error("Excel Yüklendi. " + updateCount + " Güncellendi, " + insertCount + " Kayıt Eklendi.", 2);
         }

      }


   }

   useEffect(() => {
      uploadExcel();
   }, [excelData]);
   return (
      <div>
         <div className="list-head">
            <div className="list-title">
               <h1>{module ? module.name : ""} </h1>
               <p><font face="tahoma"
                        size="1"> {module&&module.name === "Kullanıcılar" ? "  (Toplam Kullanıcı " + activeUserCount + " , Aktif Kullanıcı : " + loginUserCount + " , Adminler : " + adminCount + ")" : ""}</font>
               </p>
            </div>
            <div className="list-buttons">
               {/* <Button type="primary" icon={<FileMarkdownOutlined />} size="large" style={{ marginRight: "5px" }} onClick={() => getReport()}>Giriş Logları İndir</Button> */}
               {/* {!isSmall && <Button type="danger" icon={<DeleteOutlined/>} size="large" onClick={deleteSelectedRows}
                       style={{marginRight: "5px"}}>Seçilenleri Sil</Button>} */}
               {/* <Link to="/registeredUsers/add" style={{ marginRight: "5px" }}>
            <Button type="light" icon={<PlusOutlined />} size="large">Yeni Ekle</Button>
          </Link> */}
               <Button size="large" onClick={downloadExcel} style={{marginRight: "5px"}} icon={<DownloadOutlined/>}>{!isSmall && 'Export Excel'}</Button>

               {/* <ImportExcel setExcelData={setExcelData} /> */}

            </div>
         </div>

         <div className="table-wrap">
            <Search placeholder="isim-Email-Telefon Ara"onChange={filter} />
            <Table dataSource={data} columns={columns}
                   loading={{spinning: loading, indicator: <Loading3QuartersOutlined spin/>, size: "large"}}
                   onChange={handleTableChange}
                   pagination={{
                      total: totalCount
                   }}
                   //rowSelection={!isSmall && {...rowSelection}}
                   />
         </div>
      </div>
   );
};


export default User;
