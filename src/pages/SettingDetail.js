import React, { useEffect, useState } from "reactn";

import { useParams } from 'react-router-dom'
import moment from 'moment';
import { Row,Col,Form, Input, Button, Switch, DatePicker, Card, message ,Select, InputNumber} from 'antd';
import { useHistory } from 'react-router-dom';
import { LeftOutlined } from "@ant-design/icons";
import { ImageUrl } from '../components';

import ReactQuill from 'react-quill';

import api from '../service/index'
import { Link } from 'react-router-dom';

let formLayout = {
  labelCol: {
    xs: { span: 4 },
  },
  wrapperCol: {
    flex: true,
    xs: { span: 16 },
    sm: { span: 20 },
    md: { span: 20 },
    lg: { span: 12 },
    xl: { span: 10 },
  },
};

const SettingDetail = (props) => {

  let params = useParams()
  let history = useHistory()
  let id = params.id !== "add" ? params.id : false;

  let [data, setData] = useState({});
  let [errors, setErrors] = useState([]);
  let [loading, setLoading] = useState(true);
  let [isSmall, setIsSmall] = useGlobal('isSmall')

  //onetime run
  useEffect(() => {
    
    get();
  }, []);

  let get = async () => {
    if(id) {
      await api.get("/rest/modules/" + id).then(({ data: { result, result_message } }) => {
        setData(result);   
        setLoading(false)
      });
    } else {
      setLoading(false)
    }
  }
  
  let save = async () => {
      if(id) {
        api.put("/rest/modules/" + id, data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Bilgiler güncellendi", 2);
            history.push('/setting')
          }
          else
            message.error("Kayıt eklenirken bir hata oluştu.", 2);
        })
      } else {
        api.post("/rest/modules", data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
            history.push('/setting')
          } else {
            message.error(result_message.message, 2);
          }
        })
      }       
  };

  const { Option } = Select;

  return (
    <div>
      <div className="list-head">
        <div className="list-title">
          <h1>Modüller</h1>
        </div>
        <div className="list-buttons">
          <Link to="/setting">
            <Button type="light" icon={<LeftOutlined />} size="large">{!isSmall&&"GERİ"}</Button>
          </Link>
        </div>
      </div>
      <div className="form-wrap">
        {!loading &&
          <Card title={id ? "Modül Düzenle" : "Modül Ekle"}>
            <Form layout="horizontal" size={"large"} onFinish={save}>


            <Row direction="row">
              <Col xs={{ span: 24 }} md={{ span: 24 }}>
                <Form.Item label="Modül Adı" help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                  <Input name="name" value={data.name} onChange={e => setData({ ...data, name: e.target.value })} />
                </Form.Item>
              </Col>
            </Row>

            <Row direction="row">
               <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Adminde Göster" help={errors.adminShow} validateStatus={errors.adminShow ? 'error' : 'success'}>
                  <Switch checked={data.adminShow ? true : false} checkedChildren="Göster" unCheckedChildren="Gösterme" onChange={v => setData({ ...data, adminShow: v })} />
                </Form.Item>
              </Col>
              <Col xs={{ span: 24 }} md={{ span: 12 }}>
              <Form.Item label="Uygulamada Göster" help={errors.appShow} validateStatus={errors.appShow ? 'error' : 'success'}>
                <Switch checked={data.appShow ? true : false} checkedChildren="Göster" unCheckedChildren="Gösterme" onChange={v => setData({ ...data, appShow: v })} />
              </Form.Item>
              </Col>
            </Row>

            <Row direction="row">
               <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Versiyon" help={errors.version} validateStatus={errors.version ? 'error' : 'success'}>
                  <Input name="version" value={data.version} onChange={e => setData({ ...data, version: e.target.value })} />
                </Form.Item>
              </Col>
              {/* <Col xs={{ span: 24 }} md={{ span: 12 }}>
                 <Form.Item label="Dil" help={errors.lang} validateStatus={errors.version ? 'error' : 'success'}>
                  <Input name="lang" value={data.lang} onChange={e => setData({ ...data, lang: e.target.value })} />
                 </Form.Item>
              </Col> */}
            </Row>

            <Row direction="row">
               <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Arkaplan Rengi" help={errors.backgroundColor} validateStatus={errors.backgroundColor ? 'error' : 'success'}>
                  <Input name="backgroundColor" value={data.backgroundColor} onChange={e => setData({ ...data, backgroundColor: e.target.value })} />
                </Form.Item>
              </Col>
              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                 <Form.Item label="Yazı Renk" help={errors.textColor} validateStatus={errors.textColor ? 'error' : 'success'}>
                  <Input name="name" value={data.textColor} onChange={e => setData({ ...data, textColor: e.target.value })} />
                 </Form.Item>
              </Col>
            </Row>

            <Row direction="row">
               <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Sıra" help={errors.order} validateStatus={errors.order ? 'error' : 'success'}>
                  <Input name="order" type="number" value={data.order} onChange={e => setData({ ...data, order: e.target.value })} />
                </Form.Item>
              </Col>
              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                 <Form.Item label="Type" help={errors.type} validateStatus={errors.type ? 'error' : 'success'}>
                  <Input name="type" value={data.type} onChange={e => setData({ ...data, type: e.target.value })} />
                 </Form.Item>
              </Col>
            </Row>
            <Row direction="row">
              <Col span={24}>
                <Form.Item label="İkon" help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                  <ImageUrl record={data} setRecord={setData} name="icon" />
                </Form.Item>
              </Col>
            </Row>
            
            <Row direction="row">
              <Col span={24}>
              <Form.Item>
                  <Button type="primary" htmlType="submit" size="large" block > KAYDET </Button>
              </Form.Item>
              </Col>
            </Row>
           
            </Form>
          </Card>
        }
      </div>
    </div>
  );
};



export default SettingDetail;
