import React, {useEffect, useState, useGlobal} from "reactn";
import {useParams} from 'react-router-dom'
import {Row, Col, Form, Button, Card, message, Typography, Image, Input, Switch} from 'antd';
import {useHistory} from 'react-router-dom';
import {LeftOutlined, CheckOutlined, CloseOutlined} from "@ant-design/icons";
import api from '../service/index'
import {Link} from 'react-router-dom';

const CollectionDetail = (props) => {

    const {TextArea} = Input;
    const {Paragraph, Text,} = Typography;
    let params = useParams()
    let history = useHistory()
    let id = params.id !== "add" ? params.id : false;
    let newRecord = {}
    let [data, setData] = useState(id ? {} : newRecord);
    let [loading, setLoading] = useState(id ? true : false);

    let [modules] = useGlobal("modules");
    let [pdfIcon] = useGlobal("pdfIcon");
    let [isSmall, setIsSmall] = useGlobal('isSmall')

    let path = props.location.pathname.split('/')[1];
    let module
    if (modules !== null) {
        [module] = modules.filter(el => el._id === path);
    }
    useEffect(() => {
        if (modules && id) {
            setLoading(true)
            api.get("/rest/paymentLog/" + id).then(({data: {result, result_message}}) => {
                console.log(result)
                setData(result);
                setLoading(false)
            });
        }
    }, [id]);
    return (
        <div>
            <div className="list-head">
                <div className="list-title">
                    <h1>{module ? module.name : ""}</h1>
                </div>
                <div className="list-buttons">
                    <Link to={"/collection"}>
                        <Button type="light" icon={<LeftOutlined/>} size="large">{!isSmall && "GERİ"}</Button>
                    </Link>
                </div>
            </div>
            <div className="form-wrap">
                {!loading &&
                    <Card title={id ? "" : ""} loading={loading}>
                        <Row direction="row">
                            <Col xs={{span: 24}} xl={12} style={{maxWidth: "70%", wordWrap: "break-word"}}>
                                <Typography.Title style={{padding: 0, margin: 0, whiteSpace: "normal"}}
                                                  level={5}>Ad Soyad
                                    : {data.name + " " + data.lastname}</Typography.Title>
                            </Col>
                            <Col xs={{span: 24}}>
                                <Typography.Text>Email
                                    : {data.email}</Typography.Text>
                            </Col>
                            <Col xs={{span: 24}}>
                                <Typography.Text>Telefon
                                    : {data.phone}</Typography.Text>
                            </Col>
                            <Col xs={{span: 24}}>
                                <Typography.Text>TC
                                    : {data.tcNo}</Typography.Text>
                            </Col>
                            <Col xs={{span: 24}}>
                                <Typography.Text>Vergi No
                                    : {data.vergiNo ? data.vergiNo : "-"}</Typography.Text>
                            </Col>
                            <Col xs={{span: 24}} style={{marginBottom: "20px"}}>
                                <Typography.Text>Toplam Alınan Ücret : {data.total}</Typography.Text>
                            </Col>
                            <Col xs={{span: 24}} xl={16} md={16} style={{padding: 0}}>
                                {
                                    data.bills.map((bill, i) => (
                                        <>
                                            <hr/>
                                            <Row style={{display: "block"}}>
                                                <Col xs={{span: 24}} xl={12}
                                                     style={{maxWidth: "100%", wordWrap: "break-word"}}>
                                                    <Typography.Title
                                                        style={{padding: 0, margin: 0, whiteSpace: "normal"}}
                                                        level={5}>{i + 1}.Fatura </Typography.Title>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col xs={{span: 24}}><Typography.Text>Fatura No
                                                    : {bill.FATURA_NUMARASI}</Typography.Text> </Col>
                                                <Col xs={{span: 24}}><Typography.Text>Sozlesme Hesap No
                                                    : {bill.SOZLESME_HESAP_NO}</Typography.Text></Col>
                                                <Col xs={{span: 24}}><Typography.Text>Son Ödeme Tarihi
                                                    : {bill.SON_ODEME}</Typography.Text></Col>
                                                <Col xs={{span: 24}}><Typography.Text>Tutar
                                                    : {bill.TUTAR}</Typography.Text></Col>
                                                {bill.BankReferenceDocumentNumber && (
                                                    <>
                                                        <Col xs={{span: 24}}><Typography.Text>Banka Referans Doküman
                                                            Numarası
                                                            : {bill.BankReferenceDocumentNumber}</Typography.Text></Col>
                                                        <Col xs={{span: 24}}><Typography.Text>İşlem
                                                            sonucu: {bill.message ? bill.message : "-"}</Typography.Text></Col>
                                                    </>
                                                )}
                                                {
                                                    data.resultStatus === "SUCCESS" && data.company === "Moka" && (
                                                        <Col
                                                            xs={{span: 24}}><Typography.Text>{data.resultMessage === "" ? "Ödeme işlemi başarıyla tamamlandı" : data.resultMessage}</Typography.Text></Col>
                                                    )
                                                }
                                                {
                                                    data.resultStatus === "SUCCESS" && data.company === "Octet" && (
                                                        <Col xs={{span: 24}}><Typography.Text>{data.resultMessage}</Typography.Text></Col>
                                                    )
                                                }


                                            </Row>
                                        </>
                                    ))
                                }
                            </Col>
                        </Row>
                    </Card>
                }
            </div>
        </div>
    );
};


export default CollectionDetail;
