import React, { useEffect, useState, useGlobal } from "reactn";
import { useParams } from 'react-router-dom'
import moment from 'moment';
import { Row, Col, Form, Input, Button, Switch, DatePicker, Card, message, Select,Alert } from 'antd';
import { useHistory } from 'react-router-dom';
import { LeftOutlined } from "@ant-design/icons";
import { CityPicker, FileInputSingle } from '../components';
import api from '../service/index'
import { Link } from 'react-router-dom';
import { useCallback } from "react";
import { env } from '../app.config'

const UserDetail = (props) => {

  let params = useParams()
  let history = useHistory()
  let id = params.id !== "add" ? params.id : false;
  let settings = useGlobal("settings")[0]
  let profileSettings;
  let [langs] = useGlobal("langs")
  let [isSmall, setIsSmall] = useGlobal('isSmall')
  if (settings) {
    settings.forEach(element => {
      if (element._id === "profileSettings") {
        profileSettings = element
      }
    });
  }

  let newRecord = {
    name: '',
    lastname: '',
    role: 'user',
    groups: [],
    phone: '',
    email: '',
    company: '',
    department: '',
    position: '',
    jobPhone: '',
    facebook: '',
    twitter: '',
    instagram: '',
    linkedin: '',
    skype: '',
    lang: langs ? langs.code : "TR",
  }
  let loginType = env.LOGINTYPE
  let [data, setData] = useState(id ? [] : newRecord);
  let [errors, setErrors] = useState([]);
  let [loading, setLoading] = useState(id ? true : false);
  let [validationCheck, setValidationCheck] = useState(false)
 
  let [modules] = useGlobal("modules");
  let path = props.location.pathname.split('/')[1];
  let module
  if(modules !== null){
     [module] = modules.filter(el => el._id === path);
  }

  let passMessage = ""

  if(loginType === "phone")
    passMessage = "- Uygulama girişlerinde telefonunuza kod gelecektir. Bu alanı Admin kulalnıcılarının Cms'e girebilmesi için belirlemelisiniz."

  //onetime run
  useEffect(() => {
    if (modules && id) {
      setLoading(true)
        api.get("/rest/registeredUsers/" + id).then(({ data: { result, result_message } }) => {
          if (id) { result.sendSms = false }
          delete result.password;
          setData(result);
          setLoading(false)
        });
    }
  }, [id]);

  let validate = useCallback(() => {
    let errors = {};

    if (data.name === null || data.name.length === 0)
      errors.name = 'Zorunlu Alan!'

    if (data.lastname === null || data.lastname.length === 0)
      errors.lastname = 'Zorunlu Alan!'

    if (loginType === "phone") {
      if (data.phone === null || data.phone.length === 0)
        errors.phone = 'Zorunlu Alan!'
    }
    else if (loginType === "emailPass") {
      if (data.email === null || data.email.length === 0)
        errors.email = 'Zorunlu Alan!'
    }
    else {
      if (data.email === null || data.email.length === 0)
        errors.email = 'Zorunlu Alan!'
      if (data.phone === null || data.phone.length === 0)
        errors.phone = 'Zorunlu Alan!'
    }
    errors.all = Object.getOwnPropertyNames(errors).map(n => errors[n]);
    errors.hasError = errors.all.length > 0;
    return errors;
  }, [data]);

  useEffect(() => { if (validationCheck) setErrors(validate()) }, [validationCheck, data, validate]);


  let save = async () => {
    setValidationCheck(true)
    let err = validate()
    if (err.hasError) {
      setErrors(err)
      window.scrollTo({ top: 20, behavior: 'smooth' });
    }
    else {

      if (id) {
        api.put("/rest/registeredUsers/" + id, data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Bilgiler güncellendi", 2);
            history.push('/registeredUsers')
          }
          else
            message.error(result_message.message, 2);
        })
      } else {
        api.post("/rest/registeredUsers", data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
            history.push('/registeredUsers')
          } else {
            message.error(result_message.message, 2);
          }
        })
      }
    }
  };

  const { Option } = Select;

  return (
    <div>
      <div className="list-head">
        <div className="list-title">
          <h1>{module ? module.name : ""}</h1>
        </div>
        <div className="list-buttons">
          <Link to="/registeredUsers">
            <Button type="light" icon={<LeftOutlined />} size="large">{!isSmall&&"GERİ"}</Button>
          </Link>
        </div>
      </div>
      <div className="form-wrap">
          <Card title={id ? "Düzenle" : "Ekle"} loading={loading}>
            <Form layout="horizontal" size={"large"} onFinish={save}>
              <Row direction="row">
                <Col span={12}>
                  <Form.Item label="Avatar" help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                    <FileInputSingle record={data} setRecord={setData} name="avatar" />
                    <Alert  message="Avatar 1024 x 1024 çözünürlüğünde olmalıdır." banner />
                  </Form.Item>
                </Col>
              </Row>
              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Rol" required help={errors.role} validateStatus={errors.role ? 'error' : 'success'}>
                    <Select defaultValue={(data.role) ? data.role : 'user'} style={{ width: 120 }} onChange={v => { setData({ ...data, role: v }) }}>
                      <Option value="user">User</Option>
                      < Option value="admin">Admin</Option>
                    </Select>
                    <br /><small> {"Admin kullanıcıları uygulama üzerindeki listelerde gözükmez!"}</small>
                  </Form.Item>
                </Col>
              </Row>
              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Eposta" required={loginType === "emailPass" ? true : false} help={errors.email} validateStatus={errors.email ? 'error' : 'success'}>
                    <Input name="email" value={data.email} onChange={e => setData({ ...data, email: e.target.value })} />
                  </Form.Item>
                </Col>
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Telefon" required={loginType === "phone" ? true : false} help={errors.phone} validateStatus={errors.phone ? 'error' : 'success'}>
                    <Input name="phone" value={data.phone} onChange={e => setData({ ...data, phone: e.target.value })} />
                  </Form.Item>
                </Col>
              </Row>
              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="İsim" required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                    <Input name="name" value={data.name} onChange={e => setData({ ...data, name: e.target.value })} />
                  </Form.Item>
                </Col>
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Soysim" required help={errors.lastname} validateStatus={errors.lastname ? 'error' : 'success'}>
                    <Input name="lastname" value={data.lastname} onChange={e => setData({ ...data, lastname: e.target.value })} />
                  </Form.Item>
                </Col>
              </Row>
              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="Yeni Şifre Belirle" help={errors.password} validateStatus={errors.password ? 'error' : 'success'}>
                    <Input name="password" onChange={e => setData({ ...data, password: e.target.value })} />
                    <br /><small>- Kullanıcı Şifresini değiştirmemek için boş bırakınız</small>
                    <br /><small>{passMessage}</small>
                  </Form.Item>
                </Col>
                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                  <Form.Item label="SMS Gönder" help={errors.sendSms} validateStatus={errors.sendSms ? 'error' : 'success'}>
                    <Switch checked={data.sendSms ? true : false} checkedChildren="Gönder" unCheckedChildren="Gönderme" onChange={v => setData({ ...data, sendSms: v })} />
                  </Form.Item>
                </Col>
              </Row>
              <Row direction="row">
                {profileSettings.showCompany === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Şirket" help={errors.company} validateStatus={errors.company ? 'error' : 'success'}>
                      <Input name="company" value={data.company} onChange={e => setData({ ...data, company: e.target.value })} />
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showDepartment === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Departman" help={errors.department} validateStatus={errors.department ? 'error' : 'success'}>
                      <Input name="department" value={data.department} onChange={e => setData({ ...data, department: e.target.value })} />
                    </Form.Item>
                  </Col>
                }
              </Row>
              <Row direction="row">
                {profileSettings.showPosition === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Ünvan" help={errors.position} validateStatus={errors.position ? 'error' : 'success'}>
                      <Input name="position" value={data.position} onChange={e => setData({ ...data, position: e.target.value })} />
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showJobPhone === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="İş Telefonu:" help={errors.jobPhone} validateStatus={errors.jobPhone ? 'error' : 'success'}>
                      <Input name="jobPhone" value={data.jobPhone} onChange={e => setData({ ...data, jobPhone: e.target.value })} />
                    </Form.Item>
                  </Col>
                }
              </Row>
              <Row direction="row">
                {profileSettings.showJobStartDate === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="İşe Başlama Tarihi:" help={errors.jobStartDate} validateStatus={errors.jobStartDate ? 'error' : 'success'}>
                      <DatePicker defaultValue={() => moment(data.jobStartDate)} onChange={v => setData({ ...data, jobStartDate: v })} format='DD/MM/YYYY HH:mm' showTime={true} />
                      <br /><small>İşe Başlama tarihi içerisinde bulunan günün tarihi ise varsayılan tarihtir değiştirilmediği sürece kaydedilmez. </small>
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showLocation === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Şehir:" help={errors.location} validateStatus={errors.location ? 'error' : 'success'}>
                      <CityPicker record={data} setRecord={setData} name="location" mode="single" />
                    </Form.Item>
                  </Col>
                }
              </Row>
              <Row direction="row">
                {profileSettings.showBirthday === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Doğum Günü" help={errors.birthday} validateStatus={errors.birthday ? 'error' : 'success'}>
                      <DatePicker defaultValue={() => moment(data.birthday)} onChange={v => setData({ ...data, birthday: v })} format='DD-MM' showTime={true} />
                      <br /><small>Doğum günü tarihi değiştirilmediği sürece kaydedilmez. Fakat düzenlemelerde içerisinde bulunan gün varsayılan olarak gözükür. </small>
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showBlood === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Kan Tipi:" help={errors.blood} validateStatus={errors.blood ? 'error' : 'success'}>
                      <Select defaultValue={(data.blood) ? data.blood : 'Kan Tipi Seçiniz'} style={{ width: 250 }} onChange={v => { setData({ ...data, blood: v }) }}>
                        <Select.Option value="A Rh+">A Rh+</Select.Option>
                        <Select.Option value="A Rh-">A Rh-</Select.Option>
                        <Select.Option value="B Rh+">B Rh+</Select.Option>
                        <Select.Option value="B Rh-">B Rh-</Select.Option>
                        <Select.Option value="0 Rh+">0 Rh+</Select.Option>
                        <Select.Option value="0 Rh-">0 Rh-</Select.Option>
                        <Select.Option value="AB Rh+">AB Rh+</Select.Option>
                        <Select.Option value="AB Rh-">AB Rh-</Select.Option>
                      </Select>
                    </Form.Item>
                  </Col>
                }
              </Row>
              <Row direction="row">
                {profileSettings.showFacebook === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Facebook:" help={errors.facebook} validateStatus={errors.facebook ? 'error' : 'success'}>
                      <Input name="facebook" value={data.facebook} onChange={e => setData({ ...data, facebook: e.target.value })} />
                      <br /><small>Sadece kullanıcı adınızı giriniz.</small>
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showTwitter === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Twitter:" help={errors.twitter} validateStatus={errors.twitter ? 'error' : 'success'}>
                      <Input name="twitter" value={data.twitter} onChange={e => setData({ ...data, twitter: e.target.value })} />
                      <br /><small>Sadece kullanıcı adınızı giriniz.</small>
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showInstagram === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Instagram:" help={errors.instagram} validateStatus={errors.instagram ? 'error' : 'success'}>
                      <Input name="instagram" value={data.instagram} onChange={e => setData({ ...data, instagram: e.target.value })} />
                      <br /><small>Sadece kullanıcı adınızı giriniz.</small>
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showLinkedin === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Linkedin:" help={errors.linkedin} validateStatus={errors.linkedin ? 'error' : 'success'}>
                      <Input name="linkedin" value={data.linkedin} onChange={e => setData({ ...data, linkedin: e.target.value })} />
                      <br /><small>Sadece kullanıcı adınızı giriniz.</small>
                    </Form.Item>
                  </Col>
                }
                {profileSettings.showSkype === true &&
                  <Col xs={{ span: 24 }} md={{ span: 12 }}>
                    <Form.Item label="Skype:" help={errors.skype} validateStatus={errors.skype ? 'error' : 'success'}>
                      <Input name="Skype" value={data.skype} onChange={e => setData({ ...data, skype: e.target.value })} />
                      <br /><small>Sadece kullanıcı adınızı giriniz.</small>
                    </Form.Item>
                  </Col>
                }
              </Row>
              <Row direction="row">
                <Col span={24}>
                  <Form.Item>
                    <Button type="primary" disabled={loading} htmlType="submit" size="large" block > KAYDET </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Card>
      </div>
    </div>
  );
};
export default UserDetail;
