import React, {useEffect, useState, useGlobal} from "reactn";
import {Table, Space, Button, Input, Popconfirm, Row, Col} from 'antd';
import api from '../service/index'
import qs from 'qs';
import moment from 'moment';
import ExportJsonExcel from 'js-export-excel';
import {Link} from 'react-router-dom';
import {
    CheckOutlined,
    FileTextOutlined,
    CloseOutlined,
    Loading3QuartersOutlined,
    DownloadOutlined,
} from "@ant-design/icons";

const Collection = (props) => {
    const {Search} = Input;
    let [data, setData] = useState([]);
    let [totalCount, setTotalCount] = useState(0)
    let [, setSelectedRows] = useState([])
    let [loading, setLoading] = useState(false)
    let [isSmall,] = useGlobal('isSmall')
    let [unchangedData, setUnchangedData] = useState([])
    let [modules] = useGlobal("modules");
    let path = props.location.pathname.split('/')[1];
    let module
    if (modules !== null) {
        [module] = modules.filter(el => el._id === path);
    }

    let handleTableChange = async (page, filters, sorter) => {
        get(page, filters, sorter);
    }

    let get = async (page, filters, sorter) => {
        page = {page: 1, pageSize: 10000000}
        let shortString = 'name';
        setLoading(true)
        if (sorter) {
            if (Object.keys(sorter).length) {
                shortString = (sorter.order === 'descend' ? '-' : '') + sorter.field
            }
        }

        let _params = {sort: shortString, ...page};

        let restData = await api.get(`/rest/paymentLog?${qs.stringify(_params)}`, {_params}).then(({data}) => {
            if (data.result.rows) {
                setLoading(false)
                setTotalCount(data.result.rows.length);
                return data.result.rows.map((item, key) => {
                    if (item.resultStatus === "SUCCESS")
                        item.resultStatus = <CheckOutlined style={{color: "#7cb305", fontSize: "1.2rem"}}/>;
                    else if (item.resultStatus === "ERROR")
                        item.resultStatus = <CloseOutlined style={{color: "#f5222d", fontSize: "1.2rem"}}/>;
                        // else if (item.action === "PaymentClick")
                    //     item.resultStatus = "Ödeme Yöntemi seçti";
                    else if (item.action === "SendPayment")
                        item.resultStatus = item.bills.length > 0 && item.bills.map((e, i) => <div key={i}
                                                                                                   style={{marginBottom: 5}}>{e.message == "I-0002: Ödeme işlemi gerçekleşti." ?
                            <CheckOutlined style={{color: "#7cb305", fontSize: "1.2rem"}}/> :
                            <CloseOutlined style={{color: "#f5222d", fontSize: "1.2rem"}}/>}</div>)
                    else if (item.action === "Payment" && item.company === "notSelected")
                        item.resultStatus = "Ödeme başlangıcı";
                    item.key = key;
                    return item;
                })
            }
        });
        setData(restData);
        setLoading(false);
        setUnchangedData(restData)

    }

    useEffect(() => {
        if (modules) {
            setLoading(true)
            get();
        }
    }, [])
    console.log(data)
    let rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelectedRows(selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            setSelectedRows(selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            setSelectedRows(selectedRows);
        },
    };

    const downloadExcel = async () => {
        var option = {};
        let dataTable = [];
        let query = []

        let excelData = await api.get(`/rest/paymentLog?${qs.stringify({
            page: 1,
            pageSize: 10000,
            query
        })}`, {}).then(({data}) => {
            data.result.rows = data.result.rows.filter(x => x.action !== "PaymentClick")
            return data.result.rows;
        });

        console.log(excelData);

        if (excelData) {
            for (let i in excelData) {
                if (excelData) {
                    let data = excelData[i]
                    console.log(data)
                    let user = data.user.split(",")
                    let obj = {
                        "Sözleşme Hesap No": !!user[7] ? user[7] : " - ",
                        "Fatura No": !!user[8] ? user[8] : " - ",
                        'Ödeme Sistemi': !!data.company ? data.company : " - ",
                        'Ücret': !!user[3] ? user[3] : " - ",
                        'İşlem Tarihi': !!data.updatedAt ? data.updatedAt : " - ",
                        'Ödeme Durumu': !!data.resultStatus ? data.resultStatus : " - ",

                    }
                    dataTable.push(obj);
                }
            }
        }
        let sheetAreas = ['Sözleşme Hesap No', 'Fatura No', 'Ödeme Sistemi', 'Ücret', 'İşlem Tarihi', 'Ödeme Durumu'];
        option.fileName = 'PaymentReport'
        option.datas = [
            {
                sheetData: dataTable,
                sheetName: 'sheet',
                sheetFilter: sheetAreas,
                sheetHeader: sheetAreas,
            }
        ];

        var toExcel = new ExportJsonExcel(option);
        toExcel.saveExcel();
    }

    const filter = (e) => {
        const val = e.target.value.toLocaleLowerCase();
        const tempList = Object.assign([], unchangedData);
        if (val === '' || !val) {
            setData(unchangedData)
            return;
        }
        let filteredData = tempList.filter(t => isContainsFilterValue(t, val))
        setData(filteredData)
        setTotalCount(filteredData.length)
    }
    const isContainsFilterValue = (t, val) => {
        const isContains1 = t.company == null ? false : t.company.toLowerCase().indexOf(val) !== -1;
        const isContains2 = t.name == null ? false : t.name.toLowerCase().indexOf(val) !== -1;
        const isContains3 = t.lastname == null ? false : t.lastname.toLowerCase().indexOf(val) !== -1;
        const isContains4 = t.tcNo == null ? false : t.tcNo.toLowerCase().indexOf(val) !== -1;
        const isContains5 = t.vergiNo == null ? false : t.vergiNo.toLowerCase().indexOf(val) !== -1;
        const isContains6 = t.action == null ? false : t.action.toLowerCase().indexOf(val) !== -1;
        const isContains7 = t.createdAt == null ? false : moment(t.createdAt).format("YYYY-MM-DD HH:mm").indexOf(val) !== -1;

        return isContains1 || isContains2 || isContains3 || isContains4 || isContains5 || isContains6 || isContains7;
    }

    let tryAgainSendPayment = async (item_id) => {
        api.get(`/api/sendPayment/${item_id}`, ({data}) => {
        });
        //setData(newData);
        get();
    }
    let columns = [
        {
            title: 'Tc Numarası',
            dataIndex: 'tcNo',
            key: 'tcNo',
            sorter: (a, b) => a.tcNo - b.tcNo,
            render: (text, record) => (record.tcNo ? record.tcNo : " - "),
            sortDirections: ['descend', 'ascend']
        },
        {
            title: 'Vergi Numarası',
            dataIndex: 'vergiNo',
            key: 'vergiNo',
            sorter: (a, b) => a.vergiNo - b.vergiNo,
            render: (text, record) => (record.vergiNo ? record.vergiNo : " - "),
            sortDirections: ['descend', 'ascend']
        },
        {
            title: 'Adı-Soyadı',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name - b.name,
            render: (text, record) => (record.name + " " + record.lastname),
            sortDirections: ['descend', 'ascend']
        },

        {
            title: 'Tutar',
            dataIndex: 'price',
            key: 'price',
            sorter: (a, b) => a.total - b.total,
            render: (text, record) => (record.total),
            sortDirections: ['descend', 'ascend']
        },
        {
            title: 'Ödeme Durumu',
            dataIndex: 'resultStatus',
            key: 'resultStatus',
            sorter: (a, b) => a.resultStatus - b.resultStatus,
            sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Tahsilat Sistemi',
            dataIndex: 'company',
            key: 'company',
            sorter: (a, b) => a.company - b.company,
            sortDirections: ['descend', 'ascend']
        },
        {
            title: 'Ödeme Sistemi',
            dataIndex: 'paymentCompany',
            key: 'paymentCompany',
            sorter: (a, b) => a.paymentCompany - b.paymentCompany,
            sortDirections: ['descend', 'ascend']
        },
        {
            title: 'İşlem Tarihi',
            dataIndex: 'createdAt',
            key: 'createdAt',
            sortDirections: ['descend', 'ascend'],
            sorter: (a, b) => a.createdAt - b.createdAt,
            render: (text) => moment(text).format("YYYY-MM-DD HH:mm"),
        },
        {
            title: "Sözleşme Numaraları",
            dataIndex: 'bills',
            key: 'bills',
            sorter: (a, b) => a.bills - b.bills,
            sortDirections: ['descend', 'ascend'],
            render: (bills) => bills.length > 0 && bills.map((e, i) => <div key={i}
                                                                            style={{marginBottom: 5}}>{e.SOZLESME_HESAP_NO}</div>)
        },
        {
            title: "Fatura Numaraları",
            dataIndex: 'bills',
            key: 'bills',
            sorter: (a, b) => a.bills - b.bills,
            sortDirections: ['descend', 'ascend'],
            render: (bills) => bills.length > 0 && bills.map((e, i) => <div key={i}
                                                                            style={{marginBottom: 5}}>{e.FATURA_NUMARASI}</div>)
        },
        {
            title: 'Action',
            key: 'action',
            className: 'editColumn',
            width: 150,
            render: (text, record) => (
                <Space>
                    <Row direction="row">

                        <Col xs={{span: 24}}>
                            <Link to={"/collection/detail/" + record._id}>
                                <Button icon={<FileTextOutlined/>}>
                                    {!isSmall && "Görüntüle"}
                                </Button>
                            </Link>
                        </Col>
                        {record.tryAgain &&
                            <Col xs={{span: 24}}>
                                <Popconfirm
                                    onConfirm={() => tryAgainSendPayment(record._id)}
                                    title="Akedaş Tahsilat servisine tekrar göndermek istiyor musunuz?"
                                    okText="Onayla" cancelText="Vazgeç">
                                    <Button type="danger">
                                        {!isSmall && "Tahsilat Gönder"}
                                    </Button>
                                </Popconfirm>
                            </Col>
                        }

                    </Row>
                </Space>
            ),
        },

    ];
    return (
        <div>
            <div className="list-head">
                <div className="list-title">
                    <h1>{module ? module.name : ""}</h1>
                </div>
                {/*<div className="list-buttons" style={{marginBottom: "30px !important"}}>*/}
                {/*    <Button size="large" onClick={downloadExcel} style={{marginRight: "5px"}}*/}
                {/*            icon={<DownloadOutlined/>}>*/}
                {/*        {!isSmall && 'Yapılan ödemeleri indir'}</Button>*/}
                {/*</div>*/}
            </div>

            <div className="table-wrap">
                {/*<Alert size="large" style={{marginBottom: "0.5rem"}}*/}
                {/*       message="Yalnızca tamamlanan ödemelerin detaylarını görüntüleyebilirsiniz!" banner/>*/}
                <Search placeholder="Ara" onChange={filter}/>
                <Table dataSource={data} columns={columns}
                       onChange={handleTableChange}
                       loading={{spinning: loading, indicator: <Loading3QuartersOutlined spin/>, size: "large"}}
                       pagination={{
                           total: totalCount
                       }}
                       rowSelection={{...rowSelection}}/>
            </div>

        </div>
    );
};


export default Collection;
