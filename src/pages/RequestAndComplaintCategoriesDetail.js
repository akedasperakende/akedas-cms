import React, { useEffect, useState, useGlobal } from "reactn";
import { useParams } from 'react-router-dom'
import { Row, Col, Form, Input, Button, Card, message, InputNumber,Switch } from 'antd';
import { useHistory } from 'react-router-dom';
import { LeftOutlined } from "@ant-design/icons";
import api from '../service/index'
import { Link } from 'react-router-dom';
import { useCallback } from "react";

const RequestAndComplaintCategoriesDetail = (props) => {

    let params = useParams()
    let history = useHistory()
    let id = params.id !== "add" ? params.id : false;
    let [langs] = useGlobal("langs")
    
    let newRecord = {
        lang: langs ? langs.code : "TR",
        name: '',
        order: 0
    }

    let [data, setData] = useState(id ? {} : newRecord);
    let [errors, setErrors] = useState([]);
    let [loading, setLoading] = useState(id ? true : false);
    let [validationCheck, setValidationCheck] = useState(false)
    let [isSmall, setIsSmall] = useGlobal('isSmall')
  
    let [modules] = useGlobal("modules");
    let path = props.location.pathname.split('/')[1];
    let module
    if(modules !== null){
       [module] = modules.filter(el => el._id === path);
    }

    //onetime run
    useEffect(() => {
        if (modules && id) {
            setLoading(true)
                api.get("/rest/requestAndComplaintCategories/" + id).then(({ data: { result, result_message } }) => {
                    setData(result);
                    setLoading(false)
                });
        }
    }, [id]);


    let validate = useCallback(() => {
        let errors = {};


        if (data.name === null || data.name.length === 0)
            errors.name = 'Zorunlu Alan!'


        errors.all = Object.getOwnPropertyNames(errors).map(n => errors[n]);
        errors.hasError = errors.all.length > 0;
        return errors;
    }, [data]);

    useEffect(() => { if (validationCheck) setErrors(validate()) }, [validationCheck, data, validate]);

    let save = async () => {
        setValidationCheck(true)
        let err = validate()
        if (err.hasError) {
            setErrors(err)
            window.scrollTo({ top: 20, behavior: 'smooth' });
        }
        else {
            if (id) {
                api.put("/rest/requestAndComplaintCategories/" + id, data).then(({ data: { result, result_message } }) => {
                    if (result_message.type === 'success') {
                        message.success("Bilgiler güncellendi", 2);
                        history.push('/requestAndComplaintCategories')

                    }
                    else
                        message.error("Kayıt eklenirken bir hata oluştu.", 2);
                })
            } else {
                api.post("/rest/requestAndComplaintCategories", data).then(({ data: { result, result_message } }) => {
                    if (result_message.type === 'success') {
                        message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
                        history.push('/requestAndComplaintCategories')
                    } else {
                        message.error(result_message.message, 2);
                    }
                })
            }
        }
    };

    return (
        <div>
            <div className="list-head">
                <div className="list-title">
                    <h1>{module ? module.name : ""}</h1>
                </div>
                <div className="list-buttons">
                    <Link to="/requestAndComplaintCategories">
                        <Button type="light" icon={<LeftOutlined />} size="large">{!isSmall&&"GERİ"}</Button>
                    </Link>
                </div>
            </div>
            <div className="form-wrap">
                    <Card title={id ? "Düzenle" : "Ekle"} loading={loading}>
                        <Form layout="horizontal" size={"large"} onFinish={save}>
                            {/* <Row direction="row">
                                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                                    <Form.Item label="Dil">
                                        <LangPicker record={data} setRecord={setData} name="lang" />
                                    </Form.Item>
                                </Col>
                            </Row> */}
                            <Row direction="row">
                                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                                    <Form.Item label="Başlık" required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                                        <Input name="name" value={data.name} onChange={e => setData({ ...data, name: e.target.value })} />
                                    </Form.Item>
                                </Col>
                            <Row direction="row">
                               <Col xs={{ span: 24 }} md={{ span: 12 }}>
                                    <Form.Item label="Durumu" required help={errors.active} validateStatus={errors.active ? 'error' : 'success'}>
                                       <Switch checked={data.active ? true : false} checkedChildren="Aktif" unCheckedChildren="Pasif" onChange={v => setData({ ...data, active: v })} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        
                                <Col xs={{ span: 24 }} md={{ span: 12 }}>
                                    <Form.Item label="Sıra" help={errors.order} validateStatus={errors.order ? 'error' : 'success'}>
                                        <InputNumber name="order" value={data.order} onChange={v => setData({ ...data, order: v })} />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row direction="row">
                                <Col span={24}>
                                    <Form.Item>
                                        <Button type="primary" disabled={loading} htmlType="submit" size="large" block > KAYDET </Button>
                                    </Form.Item>
                                </Col>
                            </Row>

                        </Form>
                    </Card>
            </div>
        </div>
    );
};



export default RequestAndComplaintCategoriesDetail;
