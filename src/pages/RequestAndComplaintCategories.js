import React, { useEffect, useState, useGlobal } from "reactn";
import { Table, Space, Button, Input, Popconfirm, message } from 'antd';
import api from '../service/index'
import { Link } from 'react-router-dom';
import qs from 'qs';

import { EditOutlined, DeleteOutlined, SnippetsOutlined, PlusOutlined, LeftOutlined, CheckOutlined,FileExcelOutlined,
     CloseOutlined, Loading3QuartersOutlined } from "@ant-design/icons";

const RequestAndComplaintCategories = (props) => {

    const { Search } = Input;
    let [data, setData] = useState([]);
    let [totalCount, setTotalCount] = useState(0)
    let [selectedRows, setSelectedRows] = useState([])
    let [search, setSearch] = useState(false)
    let [loading, setLoading] = useState(false)
    let [isSmall, setIsSmall] = useGlobal('isSmall')

    let [modules] = useGlobal("modules");
    let path = props.location.pathname.split('/')[1];
    let module
    if (modules !== null) {
        [module] = modules.filter(el => el._id === path);
    }

    let handleTableChange = async (page, filters, sorter) => {
        get(page, filters, sorter);
    }

    let get = async (page, filters, sorter) => {
        if (page) {
            page = { page: page.current, pageSize: page.pageSize }
        } else {
            page = { page: 1, pageSize: 10 };
        }
        let shortString = 'order';

        if (sorter) {
            if (Object.keys(sorter).length) {
                shortString = (sorter.order === 'descend' ? '-' : '') + sorter.field
            }
        }

        let _params = { sort: shortString, ...page, };

        if (search.length > 2) {
            _params["search"] = search.toLowerCase();
            _params["searchFields"] = "title";
        }
        let restData = await api.get(`/rest/requestAndComplaintCategories?${qs.stringify(_params)}`, { _params }).then(({ data }) => {
            if (data.result) {
                setLoading(false)
                setTotalCount(data.result.total);
                return data.result.rows.map((item, key) => {
                    if (item.active)
                        item.active = <CheckOutlined />;
                    else
                        item.active = <CloseOutlined />;
                    item.key = key;
                    return item;
                })
            }
        });
        setData(restData);
    }

    useEffect(() => {
        if (modules) {
            setLoading(true)
            get();
        }
    }, [])

    useEffect(() => {
        get();
    }, [search]);


    let rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelectedRows(selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            setSelectedRows(selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            setSelectedRows(selectedRows);
        },
    };


    let deleteRow = async (item_id) => {
        api.delete(`/rest/requestAndComplaintCategories/${item_id}`).then(( 
            { data: { result, result_message } }) => {
                if (result_message.type == "error") message.error(result_message.message)
                else {
                    let newData = data.filter(el => el._id !== item_id);
                    setData(newData);
                }
             }) ;
        
    }
    let deleteSelectedRows = async () => {
        selectedRows.map(item => {
            api.delete(`/rest/requestAndComplaintCategories/${item._id}`, ({ data }) => { });
            let newData = data.filter(el => el._id !== item._id);
            setData(newData);
            get();
        })
    }
    let getReport = async (record) => {
        let result = await api.get(`/rest/reports/requestAndComplaintCategories?id=${record._id}`)
        const file = new Blob(["\ufeff", result.data]);
        let _url = window.URL.createObjectURL(file);
        let a = document.createElement('a');
        a.href = _url;
        a.download = `${record.name.replace(/ /g, '')}_report.xls`;
        document.body.appendChild(a);
        a.click();
        a.remove();
      }
    let onChange = async (e) => {
        setSearch(e.target.value);
        get();
    }

    let columns = [
        {
            title: 'Başlık',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name - b.name,
            sortDirections: ['descend', 'ascend']
        },
        {
            title: 'Aktif',
            dataIndex: 'active',
            key: 'active',
            sorter: (a, b) => a.active - b.active,
            sortDirections: ['descend', 'ascend']
          },
        {
            title: 'Sıra',
            dataIndex: 'order',
            key: 'order',
            sorter: (a, b) => a.order - b.order,
            sortDirections: ['descend', 'ascend']
        },
        // {
        //     title: 'Dil',
        //     dataIndex: 'lang',
        //     key: 'lang',
        //     sorter: (a, b) => a.lang - b.lang,
        //     sortDirections: ['descend', 'ascend']
        // },
        {
            title: 'Action',
            key: 'action',
            className: 'editColumn',
            width: 150,
            render: (text, record) => (
                <Space size="small">
                    <Button type="primary"  icon={<FileExcelOutlined />} onClick={() => getReport(record)}>
                        {!isSmall&&"Rapor"}
                    </Button>
                    <Link to={"/requestAndComplaintCategories/detail/" + record._id}>
                        <Button icon={<SnippetsOutlined />}>{!isSmall &&"Alt Kategorileri"}</Button>
                    </Link>
                    <Link to={"/requestAndComplaintCategories/edit/" + record._id}>
                        <Button icon={<EditOutlined />}>{!isSmall &&"Düzenle"}</Button>
                    </Link>
                    <Popconfirm
                        onConfirm={() => deleteRow(record._id)} title="Silmeyi Onaylıyor musunuz?"
                        okText="Onayla" cancelText="Vazgeç">
                        <Button type="danger" icon={<DeleteOutlined />}>
                            {!isSmall&&"Sil"}
                        </Button>
                    </Popconfirm>


                </Space>
            ),
        },
    ];

    return (
        <div>
            <div className="list-head">
                <div className="list-title">
                    <h1>{module ? module.name : ""}</h1>
                </div>
                <div className="list-buttons">
                    <Button type="danger" icon={<DeleteOutlined />} size="large" onClick={deleteSelectedRows} style={{ marginRight: "5px" }}>{!isSmall&&"Seçilenleri Sil"}</Button>
                    <Link to="/requestAndComplaintCategories/add">
                        <Button type="light" icon={<PlusOutlined />} size="large">{!isSmall&&"Yeni Ekle"}</Button>
                    </Link>

                    <Link to="/requestAndComplaint">
                        <Button type="light" icon={<LeftOutlined />} size="large" style={{ marginLeft: "5px" }} >{!isSmall&&"GERİ"}</Button>
                    </Link>
                </div>
            </div>

            <div className="table-wrap">
                <Search placeholder="Program Ara" onChange={onChange} onSearch={(v) => { setSearch(v); get() }} />
                <Table dataSource={data} columns={columns}
                    onChange={handleTableChange} loading={{ spinning: loading, indicator: <Loading3QuartersOutlined spin />, size: "large" }}
                    pagination={{
                        total: totalCount
                    }}
                    rowSelection={{ ...rowSelection }} />
            </div>

        </div>
    );
};



export default RequestAndComplaintCategories;
