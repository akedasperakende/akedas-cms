import React, { useEffect, useState, useGlobal,useMemo } from "reactn";
import { useParams } from 'react-router-dom'
import { Row, Col, Form, Input, Button, Card, Switch, message, DatePicker, TimePicker } from 'antd';
import { useHistory } from 'react-router-dom';
import { LeftOutlined } from "@ant-design/icons";
import api from '../service/index'
import { Link } from 'react-router-dom';
import { useCallback, useRef } from "react";

import { Quill } from 'react-quill';
import ImageResize from 'quill-image-resize-module-react'
Quill.register('modules/imageResize', ImageResize)

const RequestAndComplaintSubCategoriesDetail = (props) => {
  
  const quillRef = useRef(null);

  let params = useParams()
  let history = useHistory()
  let photoAlbum = params.id !== "add" ? params.id : false;
  let id = params.subid !== "add" ? params.subid : false;
  
  let [modules] = useGlobal("modules");
  let path = props.location.pathname.split('/')[1];
  let module
  if(modules !== null){
     [module] = modules.filter(el => el._id === path);
  }

  let newRecord = {
    name: '',
    active: "1",
    categoryId: params.id,
  }

  let [data, setData] = useState(id ? [] : newRecord);
  let [errors, setErrors] = useState([]);
  let [loading, setLoading] = useState(true);
  let [isSmall, setIsSmall] = useGlobal('isSmall')

  let [validationCheck, setValidationCheck] = useState(false)

  //onetime run

    useEffect(() => {
      if (modules) {
          setLoading(true)
          get();
      }
  }, [])

  let get = async () => {
    if (id) {
     let data= await api.get("/rest/requestAndComplaintSubCategories/" + id).then(({ data: { result, result_message } }) => {
        setData(result);
        
        console.log(result)
        setLoading(false)
      });
    } 
    else {

      setLoading(false)
    }
  }

  let validate = useCallback(() => {
    let errors = {};

    if (data.name == null || data.name.length === 0)
      errors.name = 'Zorunlu Alan!'


    errors.all = Object.getOwnPropertyNames(errors).map(n => errors[n]);
    errors.hasError = errors.all.length > 0;
    return errors;
  }, [data]);

  useEffect(() => { if (validationCheck) setErrors(validate()) }, [validationCheck, data, validate]);


  let save = async () => {
    setValidationCheck(true)
    let err = validate()
    if (err.hasError) {
      setErrors(err)
      window.scrollTo({ top: 20, behavior: 'smooth' });
    }
    else {
      if (id) {
        api.put("/rest/requestAndComplaintSubCategories/" + id, data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Bilgiler güncellendi", 2);
            history.push("/requestAndComplaintCategories/detail/" + photoAlbum)
          }
          else
            message.error("Kayıt eklenirken bir hata oluştu.", 2);
        })
      } else {
        api.post("/rest/requestAndComplaintSubCategories", data).then(({ data: { result, result_message } }) => {
          if (result_message.type === 'success') {
            message.success("Kayıt Başarılı bir şekilde eklendi.", 2);
            history.push("/requestAndComplaintCategories/detail/" + photoAlbum)
          } else {
            message.error(result_message.message, 2);
          }
        })
      }
    }
  };


  let imageHandler = () => {
    const input = document.createElement('input');

    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.click();

    input.onchange = async () => {

      const quill = quillRef.current.getEditor();
      const file = input.files[0];
      const formData = new FormData();

      formData.append('files_0', file);

      // Save current cursor state
      const range = quill.getSelection(true);

     // // Insert temporary loading placeholder image
     quill.insertEmbed(range.index, 'image', `${window.location.origin}/images/loading.gif`);
     console.log(`${window.location.origin}/images/loading.gif`);
     // Move cursor to right side of image (easier to continue typing)
     quill.setSelection(range.index + 1);
  
     let url = await api.post("/api/upload", formData, { headers: { 'ContenType': 'multipart/form-data'} }).then(({ data: { result, result_message } }) => {
       if (result_message.type === 'success') {
         console.log("YÜKLENDİ")
         return result[0].url;
       }
     });
     quill.deleteText(range.index, 1);
     quill.insertEmbed(range.index, 'image', url);
    };
  }
  
  const formats = [ 'header', 'font', 'size', 'bold', 'italic', 'underline', 'list', 'bullet', 'indent', 'link', 'image', 'color', 'strike', 'blockquote','width','height']
  const quillmodules = useMemo(() => ({
    toolbar: {
      container: [
        [{ header: '1' }, { header: '2' }, { header: [3, 4, 5, 6] }],
        [{ size: [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ list: 'ordered' }, { list: 'bullet' }],
        ['link', 'image', 'video'],
        ['clean'],
        ['code-block']
      ],
      handlers: {
        image: imageHandler
      }
    },
    imageResize: {
      parchment: Quill.import('parchment'),
      modules: ['Resize', 'DisplaySize']
    }
  }), [])


  return (
    <div>
      <div className="list-head">
        <div className="list-title">
          <h1>{module ? module.name : ""}</h1>
        </div>
        <div className="list-buttons">
          <Link to={"/requestAndComplaintCategories/detail/" + photoAlbum}>
            <Button type="light" icon={<LeftOutlined />} size="large">{!isSmall&&"GERİ"}</Button>
          </Link>
        </div>
      </div>
      <div className="form-wrap">
          <Card title={id ? "Düzenle" : "Ekle"} loading={loading}>
            <Form layout="horizontal" size={"large"} onFinish={save}>

              <Row direction="row">
                <Col xs={{ span: 24 }} md={{ span: 24 }}>
                  <Form.Item label="Başlık" required help={errors.name} validateStatus={errors.name ? 'error' : 'success'}>
                    <Input name="name" value={data.name} onChange={e => setData({ ...data, name: e.target.value })} />
                  </Form.Item>
                </Col>
              </Row>
              

              <Row direction="row">
              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Durumu" required help={errors.active} validateStatus={errors.active ? 'error' : 'success'}>
                  <Switch checked={data.active ? true : false} checkedChildren="Aktif" unCheckedChildren="Pasif" onChange={v => setData({ ...data, active: v })} />
                </Form.Item>
              </Col>
              </Row>
          
              <Row direction="row">
                <Col span={24}>
                  <Form.Item>
                    <Button type="primary" disabled={loading} htmlType="submit" size="large" block > KAYDET </Button>
                  </Form.Item>
                </Col>
              </Row>

            </Form>
          </Card>
      </div>
    </div>
  );
};



export default RequestAndComplaintSubCategoriesDetail;
