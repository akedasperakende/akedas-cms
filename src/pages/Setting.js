import React, { useEffect, useState } from "reactn";
import {
  Form,
  Input,
  Button,
  Card,
  Switch,
  message
} from 'antd';

import { Link } from 'react-router-dom';

import {DragOutlined,LeftOutlined, DeleteOutlined,PlusOutlined} from '@ant-design/icons';


import {FileInputSingle} from '../components';

import api from '../service/index'

import qs from 'qs';

 
 const Setting = (props) => {
     
  const initialDnDState = {
    draggedFrom: null,
    draggedTo: null,
    isDragging: false,
    originalOrder: [],
    updatedOrder: []
   }


  const [dragAndDrop, setDragAndDrop] = useState(initialDnDState);
  let [data, setData] = useState([]);
  let [modules, setModules] = useState([]);
  let [isSmall, setIsSmall] = useGlobal('isSmall')

  let _params = {sort: "order", page: 1, pageSize: 100 };

  let getResponse = async () => {

    await api.get(`/rest/modules?${qs.stringify(_params)}`).then(({data}) => {
      setModules(data.result.rows);
    });
  
}

    useEffect( () => {
      getResponse()
    }, []);


    let saveOrders = () => {
        modules.map((item, index) => {
          item.order = index;
            api.put("/rest/modules/"+item._id, item).then((result) => {
            })
        });
        message.success("Sıralama Kaydedildi.", 2);
    }

  const onDragStart = (event) => {
   const initialPosition = Number(event.currentTarget.dataset.position);
   
   setDragAndDrop({
    ...dragAndDrop,
    draggedFrom: initialPosition,
    isDragging: true,
    originalOrder: modules
   });
   
   event.dataTransfer.setData("text/html", '');
  }
  
  const onDragOver = (event) => {
   
   event.preventDefault();
   
   let newList = dragAndDrop.originalOrder;
  
   const draggedFrom = dragAndDrop.draggedFrom; 

   const draggedTo = Number(event.currentTarget.dataset.position); 
 
   const itemDragged = newList[draggedFrom];
   const remainingItems = newList.filter((item, index) => index !== draggedFrom);
 
    newList = [
     ...remainingItems.slice(0, draggedTo),
     itemDragged,
     ...remainingItems.slice(draggedTo)
    ];
     
   if (draggedTo !== dragAndDrop.draggedTo){
    setDragAndDrop({
     ...dragAndDrop,
     updatedOrder: newList,
     draggedTo: draggedTo
    })
   }
 
  }
  
  const onDrop = (event) => {
   
   setModules(dragAndDrop.updatedOrder);
   
   setDragAndDrop({
    ...dragAndDrop,
    draggedFrom: null,
    draggedTo: null,
    isDragging: false
   });
  }
 
 
  let onDragLeave = () => {
    setDragAndDrop({
    ...dragAndDrop,
    draggedTo: null
   });
   
  }
  React.useEffect( ()=>{
  }, [dragAndDrop])
  
  React.useEffect( ()=>{
   
  }, [modules])
  
     return(
       <div>
      <div className="list-head">
      <div className="list-title">
         <h1>Modüller</h1> 
      </div>
      <div className="list-buttons">
        <Link to="/setting/add">
          <Button type="light" icon={<PlusOutlined />} size = "large">{!isSmall &&"Yeni Ekle"}</Button>
        </Link>
      </div>
    </div>

    <div className="table-wrap">
    <ul className="dragDrop">
     
     {modules.map( (item, index) => {
      return(
       <li 
        key={index}
        
        data-position={index}
        draggable
        
        onDragStart={onDragStart}
        onDragOver={onDragOver}
        onDrop={onDrop}
        
        onDragLeave={onDragLeave}
        
        className={dragAndDrop && dragAndDrop.draggedTo=== Number(index) ? "dropArea" : ""}
        >
        <div >
          <DragOutlined style={{ fontSize: 20 }} />
          <span className="nameDrag">{item.name}</span>
        </div>
    
         <div className="rightButtons">
          <Switch checked={item.adminShow ? true : false} checkedChildren="Admin Görünsün" unCheckedChildren="Admin Görülmesin" onChange={v => {
            let newModules  = modules;
            newModules[index].adminShow = v
            setData(newModules);
          }} />
          <Switch checked={item.appShow ? true : false} checkedChildren="Uygulamada Görünsün" unCheckedChildren="Uygulamada Görülmesin" onChange={v => {
            let newModules  = modules;
            newModules[index].appShow = v
            setData(newModules);
          }} />
           <Link to={"/setting/"+item._id}><Button type="primary" size="medium" > {!isSmall&&"Düzenle"} </Button></Link>
         </div>
         
       </li>
      )
     })}
      
    </ul>
    <Form.Item>
    <Button type="primary" size="large" block onClick={saveOrders}> Sıralamayı Kaydet </Button>
    </Form.Item>

         </div>
         </div>
         )
 };
 
 export default Setting;
