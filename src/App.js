import React, { useEffect } from 'react';
import { setGlobal, useGlobal } from "reactn";
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Login from './pages/Login';
import Admin from './Admin';
import Cookies from 'js-cookie'
import 'antd/dist/antd.css';

setGlobal({
  isSmall: window.innerWidth <= 1024,
  collapsed: false,
  token: Cookies.get("token"),
  user: localStorage.getItem("user") === undefined ? [] : JSON.parse(localStorage.getItem("user")),
  modules: localStorage.getItem("modules") === undefined ? [] : JSON.parse(localStorage.getItem("modules")),
  settings: localStorage.getItem("settings") === undefined ? [] : JSON.parse(localStorage.getItem("settings")),
  langs: localStorage.getItem("langs") === undefined ? [] : JSON.parse(localStorage.getItem("langs")),
  logo: localStorage.getItem("logo") === undefined ? [] : JSON.parse(localStorage.getItem("logo")),
}); 

const App = () => {
  let [, setToken] = useGlobal('token');
  let [isSmall, setIsSmall] = useGlobal('isSmall')
  let [, setCollapsed] = useGlobal('collapsed');

  useEffect(() => {
    window.onresize = () => {
      setIsSmall(window.innerWidth < 1024)
    }
  }, [setIsSmall, setToken])

  useEffect(() => {
    if (isSmall)
      setCollapsed(true)
    else
      setCollapsed(false)
  }, [isSmall, setCollapsed])
  
  return (
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/" component={Admin} />
      </Switch>

    </Router>
  );
};


export default App;
