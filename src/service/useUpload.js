import { useGlobal, useState } from 'reactn';
import { message } from 'antd'
import { env } from '../app.config'
export const useUpload = () => {
	let [token] = useGlobal('token');
	let [loading, setLoading] = useState(false);
	let [result, setResult] = useState(null);
	let [error, setError] = useState(null);

	let clear = () => {
		setResult(null);
		setError(null);
		setLoading(false);
	};

	let host = env.API;

	let upload = async (files) => {
		setLoading(true);
		const formData = new FormData();
		files.forEach(file => formData.append('files', file));

		try {
			let { result, result_message } = await fetch(host + `/api/upload`, {
				method: 'POST',
				body: formData,
				headers: {
					'ContenType': 'multipart/form-data',
					authorization: `Bearer ${token}`
				}
			}).then(res => {
				if (res.status === 413)
					throw Error("Dosya çok büyük")
				else return res.json()
			});
			setLoading(false);

			if (result_message.type === 'error') {
				message.error(result_message.message)
				setError(result_message.message);
				return null;
			}
			else {
				setResult(result);
			}

			return result;
		} catch (err) {
			message.error(err.message)
			setLoading(false);
			setError(err.message);
			return null;
		}

	};

	return [upload, result, loading, error, clear];
};
