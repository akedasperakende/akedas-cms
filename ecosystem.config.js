module.exports = {
	apps: [{
		name: 'akedas-cms',
		script: 'yarn start',
		args: '',
		instances: 1,
		autorestart: true,
		watch: false,
		max_memory_restart: '8G',
		env: {
			NODE_ENV: 'development'
		},
		env_production: {
			NODE_ENV: 'production',
			PORT: 8003,
			CDN_HOST: 'https://cdn.iciletisim.app',
			LOGINTYPE: "emailPass"
		}
	}
	]
}; 

//test